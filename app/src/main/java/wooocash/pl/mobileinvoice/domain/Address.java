package wooocash.pl.mobileinvoice.domain;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Address {

    @Id
    private Long id;

    @NotNull
    private String address;

    @NotNull
    private String postCode;

    @NotNull
    private String city;

    @Generated(hash = 619147107)
    public Address(Long id, @NotNull String address, @NotNull String postCode,
            @NotNull String city) {
        this.id = id;
        this.address = address;
        this.postCode = postCode;
        this.city = city;
    }

    @Generated(hash = 388317431)
    public Address() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostCode() {
        return this.postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
