package wooocash.pl.mobileinvoice.domain;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.OrderBy;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;

import java.util.Date;
import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.converter.PropertyConverter;

import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.greendao.InvoiceItemDao;
import wooocash.pl.mobileinvoice.greendao.CurrencyDao;
import wooocash.pl.mobileinvoice.greendao.CompanyDao;
import wooocash.pl.mobileinvoice.greendao.InvoiceDao;

@Entity
public class Invoice {

    @Id
    private Long id;

    @NotNull
    private Long companyFromId;

    @ToOne(joinProperty = "companyFromId")
    private Company companyFrom;

    @NotNull
    private Long companyToId;

    @ToOne(joinProperty = "companyToId")
    private Company companyTo;

    @NotNull
    private Long currencyId;

    @ToOne(joinProperty = "currencyId")
    private Currency currency;

    private Double exchangeRate;

    private String exchangeRateNumber;

    private Date ExchangeDate;

    @NotNull
    private String invoiceNumber;

    @NotNull
    private Date invoiceDate;

    @NotNull
    private Date saleDate;

    @NotNull
    private Date paymentDate;

    @NotNull
    @Convert(converter = Invoice.PaymentMethodConverter.class, columnType = Integer.class)
    private PaymentMethod paymentMethod;

    private String additionalInfo;

    @NotNull
    private Double netTotalPrice;

    @NotNull
    private Double grossTotalPrice;

    @ToMany(referencedJoinProperty = "invoiceId")
    private List<InvoiceItem> items;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1192627946)
    private transient InvoiceDao myDao;

    @Generated(hash = 1921349474)
    public Invoice(Long id, @NotNull Long companyFromId, @NotNull Long companyToId, @NotNull Long currencyId,
            Double exchangeRate, String exchangeRateNumber, Date ExchangeDate, @NotNull String invoiceNumber,
            @NotNull Date invoiceDate, @NotNull Date saleDate, @NotNull Date paymentDate,
            @NotNull PaymentMethod paymentMethod, String additionalInfo, @NotNull Double netTotalPrice,
            @NotNull Double grossTotalPrice) {
        this.id = id;
        this.companyFromId = companyFromId;
        this.companyToId = companyToId;
        this.currencyId = currencyId;
        this.exchangeRate = exchangeRate;
        this.exchangeRateNumber = exchangeRateNumber;
        this.ExchangeDate = ExchangeDate;
        this.invoiceNumber = invoiceNumber;
        this.invoiceDate = invoiceDate;
        this.saleDate = saleDate;
        this.paymentDate = paymentDate;
        this.paymentMethod = paymentMethod;
        this.additionalInfo = additionalInfo;
        this.netTotalPrice = netTotalPrice;
        this.grossTotalPrice = grossTotalPrice;
    }

    @Generated(hash = 1296330302)
    public Invoice() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyFromId() {
        return this.companyFromId;
    }

    public void setCompanyFromId(Long companyFromId) {
        this.companyFromId = companyFromId;
    }

    public Long getCompanyToId() {
        return this.companyToId;
    }

    public void setCompanyToId(Long companyToId) {
        this.companyToId = companyToId;
    }

    public Long getCurrencyId() {
        return this.currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public Double getExchangeRate() {
        return this.exchangeRate;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getExchangeRateNumber() {
        return this.exchangeRateNumber;
    }

    public void setExchangeRateNumber(String exchangeRateNumber) {
        this.exchangeRateNumber = exchangeRateNumber;
    }

    public Date getExchangeDate() {
        return this.ExchangeDate;
    }

    public void setExchangeDate(Date ExchangeDate) {
        this.ExchangeDate = ExchangeDate;
    }

    public String getInvoiceNumber() {
        return this.invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getInvoiceDate() {
        return this.invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Date getSaleDate() {
        return this.saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public Date getPaymentDate() {
        return this.paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getAdditionalInfo() {
        return this.additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Double getNetTotalPrice() {
        return this.netTotalPrice;
    }

    public void setNetTotalPrice(Double netTotalPrice) {
        this.netTotalPrice = netTotalPrice;
    }

    public Double getGrossTotalPrice() {
        return this.grossTotalPrice;
    }

    public void setGrossTotalPrice(Double grossTotalPrice) {
        this.grossTotalPrice = grossTotalPrice;
    }

    @Generated(hash = 1751677432)
    private transient Long companyFrom__resolvedKey;

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 1649311737)
    public Company getCompanyFrom() {
        Long __key = this.companyFromId;
        if (companyFrom__resolvedKey == null
                || !companyFrom__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CompanyDao targetDao = daoSession.getCompanyDao();
            Company companyFromNew = targetDao.load(__key);
            synchronized (this) {
                companyFrom = companyFromNew;
                companyFrom__resolvedKey = __key;
            }
        }
        return companyFrom;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1394281124)
    public void setCompanyFrom(@NotNull Company companyFrom) {
        if (companyFrom == null) {
            throw new DaoException(
                    "To-one property 'companyFromId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.companyFrom = companyFrom;
            companyFromId = companyFrom.getId();
            companyFrom__resolvedKey = companyFromId;
        }
    }

    @Generated(hash = 1646900448)
    private transient Long companyTo__resolvedKey;

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 231001407)
    public Company getCompanyTo() {
        Long __key = this.companyToId;
        if (companyTo__resolvedKey == null
                || !companyTo__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CompanyDao targetDao = daoSession.getCompanyDao();
            Company companyToNew = targetDao.load(__key);
            synchronized (this) {
                companyTo = companyToNew;
                companyTo__resolvedKey = __key;
            }
        }
        return companyTo;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 189693801)
    public void setCompanyTo(@NotNull Company companyTo) {
        if (companyTo == null) {
            throw new DaoException(
                    "To-one property 'companyToId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.companyTo = companyTo;
            companyToId = companyTo.getId();
            companyTo__resolvedKey = companyToId;
        }
    }

    @Generated(hash = 1489923924)
    private transient Long currency__resolvedKey;

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 434384135)
    public Currency getCurrency() {
        Long __key = this.currencyId;
        if (currency__resolvedKey == null || !currency__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CurrencyDao targetDao = daoSession.getCurrencyDao();
            Currency currencyNew = targetDao.load(__key);
            synchronized (this) {
                currency = currencyNew;
                currency__resolvedKey = __key;
            }
        }
        return currency;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 2029286997)
    public void setCurrency(@NotNull Currency currency) {
        if (currency == null) {
            throw new DaoException(
                    "To-one property 'currencyId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.currency = currency;
            currencyId = currency.getId();
            currency__resolvedKey = currencyId;
        }
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 199173929)
    public List<InvoiceItem> getItems() {
        if (items == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            InvoiceItemDao targetDao = daoSession.getInvoiceItemDao();
            List<InvoiceItem> itemsNew = targetDao._queryInvoice_Items(id);
            synchronized (this) {
                if (items == null) {
                    items = itemsNew;
                }
            }
        }
        return items;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1727286264)
    public synchronized void resetItems() {
        items = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 2096295247)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getInvoiceDao() : null;
    }

    public PaymentMethod getPaymentMethod() {
        return this.paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public enum PaymentMethod {
        CASH(0), TRANSFER(1);

        final int id;

        PaymentMethod(int id) {
            this.id = id;
        }
    }

    public static class PaymentMethodConverter implements PropertyConverter<Invoice.PaymentMethod, Integer> {
        @Override
        public Invoice.PaymentMethod convertToEntityProperty(Integer databaseValue) {
            if (databaseValue == null) {
                return null;
            }
            for (Invoice.PaymentMethod unit : Invoice.PaymentMethod.values()) {
                if (unit.id == databaseValue) {
                    return unit;
                }
            }
            return Invoice.PaymentMethod.CASH;
        }

        @Override
        public Integer convertToDatabaseValue(Invoice.PaymentMethod entityProperty) {
            return entityProperty == null ? null : entityProperty.id;
        }
    }
}
