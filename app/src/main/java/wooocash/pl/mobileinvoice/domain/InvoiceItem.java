package wooocash.pl.mobileinvoice.domain;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.apihint.Internal;
import org.greenrobot.greendao.converter.PropertyConverter;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class InvoiceItem {

    @Id
    private Long id;

    @NotNull
    private Long invoiceId;

    @NotNull
    private String description;

    @NotNull
    private Double singleUnitPrice;

    @NotNull
    private Double count;

    @Convert(converter = UnitConverter.class, columnType = Integer.class)
    private Unit unit;

    @NotNull
    private Double taxRate;

    @NotNull
    private Double itemNetTotalPrice;

    @NotNull
    private Double itemGrossTotalPrice;

    @NotNull
    private Long currencyId;

    @Generated(hash = 754445294)
    public InvoiceItem(Long id, @NotNull Long invoiceId,
            @NotNull String description, @NotNull Double singleUnitPrice,
            @NotNull Double count, Unit unit, @NotNull Double taxRate,
            @NotNull Double itemNetTotalPrice, @NotNull Double itemGrossTotalPrice,
            @NotNull Long currencyId) {
        this.id = id;
        this.invoiceId = invoiceId;
        this.description = description;
        this.singleUnitPrice = singleUnitPrice;
        this.count = count;
        this.unit = unit;
        this.taxRate = taxRate;
        this.itemNetTotalPrice = itemNetTotalPrice;
        this.itemGrossTotalPrice = itemGrossTotalPrice;
        this.currencyId = currencyId;
    }

    @Generated(hash = 1526403644)
    public InvoiceItem() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInvoiceId() {
        return this.invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getSingleUnitPrice() {
        return this.singleUnitPrice;
    }

    public void setSingleUnitPrice(Double singleUnitPrice) {
        this.singleUnitPrice = singleUnitPrice;
    }

    public Double getCount() {
        return this.count;
    }

    public void setCount(Double count) {
        this.count = count;
    }

    public Unit getUnit() {
        return this.unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Double getTaxRate() {
        return this.taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Double getItemNetTotalPrice() {
        return this.itemNetTotalPrice;
    }

    public void setItemNetTotalPrice(Double itemNetTotalPrice) {
        this.itemNetTotalPrice = itemNetTotalPrice;
    }

    public Double getItemGrossTotalPrice() {
        return this.itemGrossTotalPrice;
    }

    public void setItemGrossTotalPrice(Double itemGrossTotalPrice) {
        this.itemGrossTotalPrice = itemGrossTotalPrice;
    }

    public Long getCurrencyId() {
        return this.currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public enum Unit {
        MD(0), szt(1), m2(2);

        final int id;

        Unit(int id) {
            this.id = id;
        }
    }

    public static class UnitConverter implements PropertyConverter<Unit, Integer> {
        @Override
        public Unit convertToEntityProperty(Integer databaseValue) {
            if (databaseValue == null) {
                return null;
            }
            for (Unit unit : Unit.values()) {
                if (unit.id == databaseValue) {
                    return unit;
                }
            }
            return Unit.szt;
        }

        @Override
        public Integer convertToDatabaseValue(Unit entityProperty) {
            return entityProperty == null ? null : entityProperty.id;
        }
    }
}
