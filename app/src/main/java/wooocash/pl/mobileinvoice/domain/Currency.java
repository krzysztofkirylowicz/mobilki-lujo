package wooocash.pl.mobileinvoice.domain;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Currency {

    @Id
    private Long id;

    @NotNull
    @Unique
    private String name;

    @NotNull
    @Unique
    private String code;

    @NotNull
    private String sign;

    @Generated(hash = 1590751811)
    public Currency(Long id, @NotNull String name, @NotNull String code,
            @NotNull String sign) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.sign = sign;
    }

    @Generated(hash = 1387171739)
    public Currency() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSign() {
        return this.sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
