package wooocash.pl.mobileinvoice.domain;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.DaoException;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.greendao.AddressDao;
import wooocash.pl.mobileinvoice.greendao.CompanyDao;

@Entity
public class Company {

    @Id
    private Long id;

    @NotNull
    private String name;

    @NotNull
    @Unique
    private String taxNumber;

    private String bankAccountNumber;

    @NotNull
    private boolean canCreateInvoices;

    @NotNull
    private Long addressId;

    @ToOne(joinProperty = "addressId")
    private Address address;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 458770942)
    private transient CompanyDao myDao;

    @Generated(hash = 1156467801)
    private transient Long address__resolvedKey;

    @Generated(hash = 105997593)
    public Company(Long id, @NotNull String name, @NotNull String taxNumber,
            String bankAccountNumber, boolean canCreateInvoices,
            @NotNull Long addressId) {
        this.id = id;
        this.name = name;
        this.taxNumber = taxNumber;
        this.bankAccountNumber = bankAccountNumber;
        this.canCreateInvoices = canCreateInvoices;
        this.addressId = addressId;
    }

    @Generated(hash = 1096856789)
    public Company() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaxNumber() {
        return this.taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getBankAccountNumber() {
        return this.bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public boolean getCanCreateInvoices() {
        return this.canCreateInvoices;
    }

    public void setCanCreateInvoices(boolean canCreateInvoices) {
        this.canCreateInvoices = canCreateInvoices;
    }

    public Long getAddressId() {
        return this.addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 489389972)
    public Address getAddress() {
        Long __key = this.addressId;
        if (address__resolvedKey == null || !address__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            AddressDao targetDao = daoSession.getAddressDao();
            Address addressNew = targetDao.load(__key);
            synchronized (this) {
                address = addressNew;
                address__resolvedKey = __key;
            }
        }
        return address;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1753883291)
    public void setAddress(@NotNull Address address) {
        if (address == null) {
            throw new DaoException(
                    "To-one property 'addressId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.address = address;
            addressId = address.getId();
            address__resolvedKey = addressId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1533027800)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCompanyDao() : null;
    }

}
