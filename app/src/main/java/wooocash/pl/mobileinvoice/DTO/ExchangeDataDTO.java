package wooocash.pl.mobileinvoice.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExchangeDataDTO {

    private String currency;
    private List<RatesDTO> rates = new ArrayList<>();

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<RatesDTO> getRates() {
        return rates;
    }

    public void setRates(List<RatesDTO> rates) {
        this.rates = rates;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Currency: " + currency + ", ");
        for (RatesDTO rate : rates) {
            sb.append(rate.toString() + System.lineSeparator());
        }
        return sb.toString();
    }
}
