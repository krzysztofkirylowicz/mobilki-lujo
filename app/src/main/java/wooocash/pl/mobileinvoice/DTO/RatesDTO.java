package wooocash.pl.mobileinvoice.DTO;

import java.util.Date;

public class RatesDTO {

    private Double mid;
    private Date effectiveDate;
    private String no;

    public Double getMid() {
        return mid;
    }

    public void setMid(Double mid) {
        this.mid = mid;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    @Override
    public String toString() {
        return "rates{" +
                "mid: " + mid +
                ", effectiveDate: " + effectiveDate +
                ", no: '" + no + '\'' + '}';
    }
}
