package wooocash.pl.mobileinvoice.main;

import android.app.Application;

import android.database.sqlite.SQLiteDatabase;

import wooocash.pl.mobileinvoice.greendao.CurrencyDao;
import wooocash.pl.mobileinvoice.greendao.DaoMaster;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.util.InitCurrenciesTask;

public class App extends Application {

    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "mobile-invoices-db", null);
        SQLiteDatabase  db = helper.getWritableDatabase();
        this.daoSession = new DaoMaster(db).newSession();
        new InitCurrenciesTask().execute(daoSession.getCurrencyDao());
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
