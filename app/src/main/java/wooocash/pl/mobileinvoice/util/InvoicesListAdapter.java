package wooocash.pl.mobileinvoice.util;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Invoice;

public class InvoicesListAdapter extends BaseAdapter {

    private Context context;
    private List<Invoice> invoices;
    private SimpleDateFormat dateFormat;

    public InvoicesListAdapter(Context context, List<Invoice> invoices) {
        this.context = context;
        this.invoices = invoices;
        this.dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    }

    @Override
    public int getCount() {
        return invoices.size();
    }

    @Override
    public Object getItem(int position) {
        return invoices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = View.inflate(context, R.layout.item_invoice_list, null);

        TextView tvMyCompanyName = (TextView)v.findViewById(R.id.myCompanyName);
        TextView tvContractorNumber = (TextView)v.findViewById(R.id.contractorName);
        TextView tvInvoiceDate = (TextView)v.findViewById(R.id.invoiceDate);
        TextView tvTotalNetAmount = (TextView)v.findViewById(R.id.totalNetAmount);
        tvMyCompanyName.setText(tvMyCompanyName.getText() + " " + invoices.get(position).getCompanyFrom().getName());
        tvContractorNumber.setText(tvContractorNumber.getText() + " " + invoices.get(position).getCompanyTo().getName());
        tvInvoiceDate.setText(tvInvoiceDate.getText() + " " + dateFormat.format(invoices.get(position).getInvoiceDate()));
        tvTotalNetAmount.setText(tvTotalNetAmount.getText() + " " + invoices.get(position).getNetTotalPrice() + " " + invoices.get(position).getCurrency().getCode());

        v.setTag(invoices.get(position).getId());
        return v;
    }
}
