package wooocash.pl.mobileinvoice.util;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Company;

public class CompaniesListAdapter extends BaseAdapter {

    private Context context;
    private List<Company> companies;

    public CompaniesListAdapter(Context context, List<Company> companies) {
        this.context = context;
        this.companies = companies;
    }

    @Override
    public int getCount() {
        return companies.size();
    }

    @Override
    public Object getItem(int position) {
        return companies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = View.inflate(context, R.layout.item_company_list, null);

        TextView tvCompanyName = (TextView)v.findViewById(R.id.companyName);
        TextView tvCompanyTaxNumber = (TextView)v.findViewById(R.id.companyTaxNumber);
        tvCompanyName.setText(tvCompanyName.getText() + " " + companies.get(position).getName());
        tvCompanyTaxNumber.setText(tvCompanyTaxNumber.getText() + " " + companies.get(position).getTaxNumber());

        v.setTag(companies.get(position).getId());
        return v;
    }
}
