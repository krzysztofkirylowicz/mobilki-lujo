package wooocash.pl.mobileinvoice.util;

import android.widget.EditText;

public class ValidationService {

    public boolean isStringNullOrEmpty(String text) {
        return text == null || text.trim().length() == 0;
    }

    public boolean isRequiredInputsFilled(EditText... inputs) {
        int inputErrorNumber = 0;

        for (EditText et : inputs) {
            if (isStringNullOrEmpty(et.getText().toString())) {
                et.setError("This field is required!");
                inputErrorNumber++;
            }
        }

        return inputErrorNumber == 0 ? true : false;
    }
}
