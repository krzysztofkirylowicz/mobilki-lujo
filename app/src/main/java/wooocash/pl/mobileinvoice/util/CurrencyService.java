package wooocash.pl.mobileinvoice.util;

import wooocash.pl.mobileinvoice.domain.Currency;
import wooocash.pl.mobileinvoice.greendao.CurrencyDao;

public class CurrencyService {

    private CurrencyDao currencyDao;

    public CurrencyService(CurrencyDao currencyDao) {
        this.currencyDao = currencyDao;
    }

    public Currency findCurrencyByCode(String code) {
        Currency currency = currencyDao.queryBuilder().where(CurrencyDao.Properties.Code.eq(code)).unique();
        return currency;
    }
}

