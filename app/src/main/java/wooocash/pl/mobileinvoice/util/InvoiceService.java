package wooocash.pl.mobileinvoice.util;

import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.greendao.query.QueryBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import wooocash.pl.mobileinvoice.domain.Company;
import wooocash.pl.mobileinvoice.domain.Currency;
import wooocash.pl.mobileinvoice.domain.Invoice;
import wooocash.pl.mobileinvoice.greendao.InvoiceDao;

public class InvoiceService {

    private ValidationService validationService;
    private InvoiceDao invoiceDao;

    public InvoiceService(InvoiceDao invoiceDao) {
        this.validationService = new ValidationService();
        this.invoiceDao = invoiceDao;
    }

    public void goneAddCompanyForm(EditText... inputs) {
        clearAddCompanyForm(inputs);
        for (EditText et : inputs) {
            et.setVisibility(View.GONE);
        }
    }

    private void clearAddCompanyForm(EditText... inputs) {
        for (EditText et : inputs) {
            et.getText().clear();
        }
    }

    public void showAddCompanyForm(EditText... inputs) {
        for (EditText et : inputs) {
            et.setVisibility(View.VISIBLE);
        }
    }

    public void goneExchangeRateForm(EditText exchangeDateInput, TextView exchangeRateTextView, TextView exchangeRateNumberTextView) {
        clearExchangeRateForm(exchangeDateInput, exchangeRateTextView, exchangeRateNumberTextView);
        exchangeDateInput.setVisibility(View.GONE);
        exchangeRateTextView.setVisibility(View.GONE);
        exchangeRateNumberTextView.setVisibility(View.GONE);
    }

    private void clearExchangeRateForm(EditText exchangeDateInput, TextView exchangeRateTextView, TextView exchangeRateNumberTextView) {
        exchangeDateInput.getText().clear();
        exchangeRateTextView.setText("Exchange rate:");
        exchangeRateNumberTextView.setText("Exchange rate number:");
    }

    public void showExchangeRateForm(EditText exchangeDateInput, TextView exchangeRateTextView, TextView exchangeRateNumberTextView) {
        exchangeDateInput.setVisibility(View.VISIBLE);
        exchangeRateTextView.setVisibility(View.VISIBLE);
        exchangeRateNumberTextView.setVisibility(View.VISIBLE);
    }

    public void getExchangeDataFromRestService(Activity activity, String currencyCode, String exchangeDate) {
        if (!validationService.isStringNullOrEmpty(currencyCode) && !validationService.isStringNullOrEmpty(exchangeDate)) {
            SimpleDateFormat currentFormat = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");

            try {
                String reformattedString = myFormat.format(currentFormat.parse(exchangeDate));
                new GetExchangeDataTask(activity).execute(currencyCode, reformattedString);
            } catch (ParseException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public double calculateExchangeRate(Double exchangeRate, String invoiceCurrencyCode, String invoiceItemCurrencyCode) {
        double exRate = 0;

        if (exchangeRate == null)
        {
            return 1;
        }
        if (invoiceCurrencyCode.equals(invoiceItemCurrencyCode))
        {
            exRate = 1;
        }
        else if (!invoiceCurrencyCode.equals("PLN") && invoiceItemCurrencyCode.equals("PLN"))
        {
            exRate = 1 / exchangeRate;
        }
        else
        {
            exRate = exchangeRate;
        }
        return exRate;
    }

    public String generateNextInvoiceNumber(long myCompanyID) {
        String nextInvoiceNumber = "";
        Calendar startDateRange = Calendar.getInstance();
        startDateRange.set(Calendar.MONTH, 0);
        startDateRange.set(Calendar.DAY_OF_MONTH, 1);
        startDateRange.set(Calendar.HOUR_OF_DAY, 0);
        startDateRange.set(Calendar.MINUTE, 0);
        startDateRange.set(Calendar.SECOND, 0);
        startDateRange.set(Calendar.MILLISECOND, 0);

        Calendar endDateRange = Calendar.getInstance();
        endDateRange.set(Calendar.MONTH, 11);
        endDateRange.set(Calendar.DAY_OF_MONTH, endDateRange.getActualMaximum(Calendar.DAY_OF_MONTH));
        endDateRange.set(Calendar.HOUR_OF_DAY, 23);
        endDateRange.set(Calendar.MINUTE, 59);
        endDateRange.set(Calendar.SECOND, 59);
        endDateRange.set(Calendar.MILLISECOND, 999);

        List<Invoice> lastInvoiceInMyCompany = invoiceDao.queryBuilder().limit(1)
                .where(InvoiceDao.Properties.CompanyFromId.eq(myCompanyID),
                InvoiceDao.Properties.InvoiceDate.between(startDateRange.getTime(), endDateRange.getTime()))
                .orderDesc(InvoiceDao.Properties.Id).list();

        Calendar today = Calendar.getInstance();
        int monthNumber = today.get(Calendar.MONTH) + 1;

        if (lastInvoiceInMyCompany.isEmpty()) {
            nextInvoiceNumber = "1/" + monthNumber + "/" + today.get(Calendar.YEAR);
        } else {
            String lastInvoiceNumber = lastInvoiceInMyCompany.get(0).getInvoiceNumber();
            int nextNumber = Integer.parseInt(lastInvoiceNumber.split("/")[0]) + 1;
            nextInvoiceNumber = nextNumber + "/" + monthNumber + "/" + today.get(Calendar.YEAR);
        }
        return nextInvoiceNumber;
    }

    public List<Invoice> findInvoices() {
        return invoiceDao.queryBuilder().orderDesc(InvoiceDao.Properties.Id).list();
    }

    public Invoice findInvoiceById(long invoiceID) {
        Invoice invoice = invoiceDao.queryBuilder().where(InvoiceDao.Properties.Id.eq(invoiceID)).unique();
        return invoice;
    }
}
