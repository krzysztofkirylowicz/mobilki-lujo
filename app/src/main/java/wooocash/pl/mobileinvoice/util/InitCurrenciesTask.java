package wooocash.pl.mobileinvoice.util;

import android.os.AsyncTask;

import java.util.List;

import wooocash.pl.mobileinvoice.domain.Currency;
import wooocash.pl.mobileinvoice.greendao.CurrencyDao;

public class InitCurrenciesTask extends AsyncTask<CurrencyDao, Integer, String> {

    @Override
    protected String doInBackground(CurrencyDao... currencyDaos) {
        CurrencyDao currencyDao = currencyDaos[0];
        List<Currency> currentCurrencies = currencyDao.loadAll();

        if (currentCurrencies.size() == 0) {
            Currency plnCurrency = new Currency();
            plnCurrency.setName("Zloty");
            plnCurrency.setCode("PLN");
            plnCurrency.setSign("zl");

            Currency eurCurrency = new Currency();
            eurCurrency.setName("Euro");
            eurCurrency.setCode("EUR");
            eurCurrency.setSign("€");

            Currency usdCurrency = new Currency();
            usdCurrency.setName("Dollar");
            usdCurrency.setCode("USD");
            usdCurrency.setSign("$");

            Currency gbpCurrency = new Currency();
            gbpCurrency.setName("British Pound");
            gbpCurrency.setCode("GBP");
            gbpCurrency.setSign("£");

            currencyDao.insert(plnCurrency);
            currencyDao.insert(eurCurrency);
            currencyDao.insert(usdCurrency);
            currencyDao.insert(gbpCurrency);
        }

        return "success";
    }
}
