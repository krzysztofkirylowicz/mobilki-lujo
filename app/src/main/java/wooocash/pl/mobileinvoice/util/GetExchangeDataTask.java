package wooocash.pl.mobileinvoice.util;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import wooocash.pl.mobileinvoice.DTO.ExchangeDataDTO;
import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.activity.AddInvoiceActivity;

public class GetExchangeDataTask extends AsyncTask<String, Void, ExchangeDataDTO> {

    private Activity activity;
    private String errorMessage;

    public GetExchangeDataTask(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected ExchangeDataDTO doInBackground(String... strings) {

        try {
            String currencyCode = strings[0];
            String exchangeDate = strings[1];
            String urlParams = currencyCode + "/" + exchangeDate;
            final String url = "http://api.nbp.pl/api/exchangerates/rates/a/" + urlParams;
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            ExchangeDataDTO exchangeDataDTO = restTemplate.getForObject(url, ExchangeDataDTO.class);
            System.out.println(exchangeDataDTO);
            this.errorMessage = null;
            return exchangeDataDTO;
        }
        catch (HttpClientErrorException e) {
            System.out.println(e.getResponseBodyAsString());
            this.errorMessage = e.getResponseBodyAsString();
        } catch (Exception e) {
            System.out.println("Exception message: " + e.getMessage());
            this.errorMessage = "Fail! No Internet connection.";
        }

        return null;
    }

    @Override
    protected void onPostExecute(ExchangeDataDTO exchangeDataDTO) {
        TextView exchangeRateTextView = (TextView) activity.findViewById(R.id.exchangeRateTextView);
        TextView exchangeRateNumberTextView = (TextView) activity.findViewById(R.id.exchangeRateNumberTextView);

        if (exchangeDataDTO != null)
        {
            exchangeRateTextView.setText("Exchange rate: " + exchangeDataDTO.getRates().get(0).getMid());
            exchangeRateNumberTextView.setText("Exchange rate number: " + exchangeDataDTO.getRates().get(0).getNo());
        } else if (this.errorMessage != null) {
            Toast toast = Toast.makeText(activity.getApplicationContext(), this.errorMessage, Toast.LENGTH_LONG);
            toast.show();
            exchangeRateTextView.setText("Exchange rate: ");
            exchangeRateNumberTextView.setText("Exchange rate number: ");
        }
    }
}
