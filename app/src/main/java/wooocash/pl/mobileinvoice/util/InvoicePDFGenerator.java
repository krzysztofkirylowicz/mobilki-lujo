package wooocash.pl.mobileinvoice.util;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;

import wooocash.pl.mobileinvoice.domain.Currency;
import wooocash.pl.mobileinvoice.domain.Invoice;
import wooocash.pl.mobileinvoice.domain.InvoiceItem;
import wooocash.pl.mobileinvoice.greendao.CurrencyDao;

public class InvoicePDFGenerator {

    private Invoice invoice;
    private Document pdfDoc;
    private OutputStream outputStream;
    private PdfWriter writer;
    private File pdfFile;
    private File docsFolder;
    private SimpleDateFormat dateFormat;
    private Font.FontFamily fontFamily;
    private CurrencyDao currencyDao;
    private InvoiceService invoiceService;

    public InvoicePDFGenerator() { }

    public InvoicePDFGenerator(CurrencyDao currencyDao, InvoiceService invoiceService) {
        this.pdfDoc = new Document(PageSize.A4, 20, 20, 20, 20);
        this.dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        this.fontFamily = Font.FontFamily.TIMES_ROMAN;
        this.currencyDao = currencyDao;
        this.invoiceService = invoiceService;
    }

    public void generate(Invoice invoice) throws Exception {
        this.invoice = invoice;
        this.docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");

        if (!docsFolder.exists()) {
            docsFolder.mkdir();
        }

        this.pdfFile = new File(docsFolder.getAbsolutePath(), invoice.getId().toString() + "-"
                + "Invoice.pdf");
        this.outputStream = new FileOutputStream(pdfFile);
        this.writer = PdfWriter.getInstance(pdfDoc, outputStream);

        pdfDoc.open();
        pdfDoc.add(invoiceInfo());
        pdfDoc.add(sellerInfo());
        pdfDoc.add(buyerInfo());
        pdfDoc.add(invoiceNumberHeader());
        pdfDoc.add(invoiceItemsInfo());
        pdfDoc.add(summaryInfo());
        pdfDoc.add(additionalInfoInfo());
        pdfDoc.add(totalInfo());
        pdfDoc.add(paymentInfo());
        pdfDoc.add(currencyExchangeInfo());
        writeBankInfo();
        writeRepresentativeInfo();
        pdfDoc.close();
    }

    private PdfPCell getCell(String text, int alignment, boolean border, int fontSize, int fontStyle) {
        PdfPCell cell = new PdfPCell(new Phrase(text, new Font(fontFamily, fontSize, fontStyle)));
        cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(alignment);
        if (!border)
        {
            cell.setBorder(PdfPCell.NO_BORDER);
        }
        return cell;
    }

    private Element invoiceInfo() throws DocumentException {
        PdfPTable invoiceInfo = new PdfPTable(2);
        float[] colWidths = new float[] { 70f, 34f};
        invoiceInfo.setWidthPercentage(colWidths, pdfDoc.getPageSize());

        invoiceInfo.setWidthPercentage(100);
        invoiceInfo.setSpacingAfter(10f);

        PdfPCell imageCell = new PdfPCell(new Phrase(""));
        imageCell.setBorder(PdfPCell.NO_BORDER);
        imageCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        invoiceInfo.addCell(imageCell);

        String invoiceAndSaleDate = "Invoice date: " + dateFormat.format(invoice.getInvoiceDate()) + "\n\n";
        invoiceAndSaleDate = invoiceAndSaleDate + "Sale date: " + dateFormat.format(invoice.getSaleDate());

        PdfPCell invoiceAndSaleDateCell = new PdfPCell(new Phrase(invoiceAndSaleDate, new Font(fontFamily, 9, Font.NORMAL)));
        invoiceAndSaleDateCell.setBorder(PdfPCell.NO_BORDER);
        invoiceAndSaleDateCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        invoiceInfo.addCell(invoiceAndSaleDateCell);

        return invoiceInfo;
    }

    private Element sellerInfo() {
        PdfPTable sellerInfo = new PdfPTable(1);
        sellerInfo.setWidthPercentage(100);
        sellerInfo.setSpacingAfter(15f);
        sellerInfo.addCell(getCell(invoice.getCompanyFrom().getName(), PdfPCell.ALIGN_LEFT, false, 9, Font.BOLD));
        sellerInfo.addCell(getCell(invoice.getCompanyFrom().getAddress().getAddress(), PdfPCell.ALIGN_LEFT, false, 9, Font.NORMAL));
        sellerInfo.addCell(getCell(invoice.getCompanyFrom().getAddress().getPostCode() + ", " + invoice.getCompanyFrom().getAddress().getCity(), PdfPCell.ALIGN_LEFT, false, 9, Font.NORMAL));
        sellerInfo.addCell(getCell("Tax number: " + invoice.getCompanyFrom().getTaxNumber(), PdfPCell.ALIGN_LEFT, false, 9, Font.NORMAL));
        return sellerInfo;
    }

    private Element buyerInfo() {
        PdfPTable buyerInfo = new PdfPTable(1);
        buyerInfo.setWidthPercentage(100);
        buyerInfo.setSpacingAfter(15f);
        buyerInfo.addCell(getCell("Buyer:", PdfPCell.ALIGN_LEFT, false, 9, Font.BOLD));
        buyerInfo.addCell(getCell(invoice.getCompanyTo().getName(), PdfPCell.ALIGN_LEFT, false, 9, Font.BOLD));
        buyerInfo.addCell(getCell(invoice.getCompanyTo().getAddress().getAddress(), PdfPCell.ALIGN_LEFT, false, 9, Font.NORMAL));
        buyerInfo.addCell(getCell(invoice.getCompanyTo().getAddress().getPostCode() + ", " + invoice.getCompanyTo().getAddress().getCity(), PdfPCell.ALIGN_LEFT, false, 9, Font.NORMAL));
        buyerInfo.addCell(getCell("Tax number: " + invoice.getCompanyTo().getTaxNumber(), PdfPCell.ALIGN_LEFT, false, 9, Font.NORMAL));
        return buyerInfo;
    }

    private Element invoiceNumberHeader() {
        PdfPTable invoiceNumberHeader = new PdfPTable(1);
        invoiceNumberHeader.setWidthPercentage(100);
        invoiceNumberHeader.setSpacingAfter(25f);
        invoiceNumberHeader.addCell(getCell("Invoice number: " + invoice.getInvoiceNumber(), PdfPCell.ALIGN_CENTER, false, 13, Font.NORMAL));
        return invoiceNumberHeader;
    }

    private Element invoiceItemsInfo() throws DocumentException {
        PdfPTable table;
        table = invoiceItemsInfoForOthersThanDebitNote();
        table.setWidthPercentage(100);
        return invoiceItemsDisplay(table);
    }

    private PdfPTable invoiceItemsInfoForOthersThanDebitNote() throws DocumentException {
        PdfPTable table = new PdfPTable(9);
        float[] colWidths = new float[] { 24f, 8f, 9f, 7f, 12f, 8.5f, 10.5f, 10.5f, 10.5f };
        table.setWidthPercentage(colWidths, pdfDoc.getPageSize());
        table.setSpacingAfter(25f);

        table.addCell(getCell("Description", PdfPCell.ALIGN_CENTER, true, 9, Font.BOLD));
        table.addCell(getCell("Currency", PdfPCell.ALIGN_CENTER, true, 9, Font.BOLD));
        table.addCell(getCell("Count", PdfPCell.ALIGN_CENTER, true, 9, Font.BOLD));
        table.addCell(getCell("Unit", PdfPCell.ALIGN_CENTER, true, 9, Font.BOLD));
        table.addCell(getCell("Single unit price", PdfPCell.ALIGN_CENTER, true, 9, Font.BOLD));
        table.addCell(getCell("Tax rate", PdfPCell.ALIGN_CENTER, true, 9, Font.BOLD));
        table.addCell(getCell("Net total", PdfPCell.ALIGN_CENTER, true, 9, Font.BOLD));
        table.addCell(getCell("Tax", PdfPCell.ALIGN_CENTER, true, 9, Font.BOLD));
        table.addCell(getCell("Gross total", PdfPCell.ALIGN_CENTER, true, 9, Font.BOLD));
        return table;
    }

    private Element invoiceItemsDisplay(PdfPTable table) {
        invoiceItemsDisplayForNormal(table);
        addCustomCellPaddingBottom(table);
        return table;
    }

    private void invoiceItemsDisplayForNormal(PdfPTable table) {
        for (InvoiceItem ii : invoice.getItems()) {
            if (invoice.getCurrencyId().equals(ii.getCurrencyId())) {
                invoiceItemDisplayWithoutExchangeCalculationForOthersThanDebitNote(ii, table);
            } else {
                invoiceItemDisplayWithExchangeCalculationForOthersThanDebitNote(ii, table);
            }
        }
    }

    private void invoiceItemDisplayWithoutExchangeCalculationForOthersThanDebitNote(InvoiceItem item, PdfPTable table) {
        Currency itemCurrency = currencyDao.load(item.getCurrencyId());
        table.addCell(getCell(item.getDescription(), PdfPCell.ALIGN_LEFT, true, 9, Font.NORMAL));
        table.addCell(getCell(itemCurrency.getCode(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
        table.addCell(getCell(item.getCount().toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
        table.addCell(getCell(item.getUnit().toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
        table.addCell(getCell(item.getSingleUnitPrice() + " " + itemCurrency.getCode(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
        table.addCell(getCell(item.getTaxRate() + "%", PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
        table.addCell(getCell(item.getItemNetTotalPrice().toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
        Double taxNumber = item.getItemGrossTotalPrice() - item.getItemNetTotalPrice();
        table.addCell(getCell(taxNumber.toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
        table.addCell(getCell(item.getItemGrossTotalPrice().toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
    }

    private void invoiceItemDisplayWithExchangeCalculationForOthersThanDebitNote(InvoiceItem item, PdfPTable table) {
        Currency itemCurrency = currencyDao.load(item.getCurrencyId());
        PdfPCell descriptionCell = getCell(item.getDescription(), PdfPCell.ALIGN_LEFT, true, 9, Font.NORMAL);
        descriptionCell.setRowspan(2);
        table.addCell(descriptionCell);
        table.addCell(getCell(itemCurrency.getCode(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
        PdfPCell countCell = getCell(item.getCount().toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL);
        countCell.setRowspan(2);
        table.addCell(countCell);
        PdfPCell unitCell = getCell(item.getUnit().toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL);
        unitCell.setRowspan(2);
        table.addCell(unitCell);
        PdfPCell singleUnitPriceCell = getCell(item.getSingleUnitPrice() + " " + itemCurrency.getCode(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL);
        singleUnitPriceCell.setRowspan(2);
        table.addCell(singleUnitPriceCell);
        PdfPCell taxRateCell = getCell(item.getTaxRate() + "%", PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL);
        taxRateCell.setRowspan(2);
        table.addCell(taxRateCell);
        Double netTotalWithoutExchange = Math.round((item.getSingleUnitPrice() * item.getCount()) * 100.0) / 100.0;
        Double grossTotalWithoutExchange = Math.round((item.getSingleUnitPrice() * item.getCount() * (1 + (item.getTaxRate() / 100))) * 100.0) / 100.0;
        table.addCell(getCell(netTotalWithoutExchange.toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
        Double taxNumber = Math.round((grossTotalWithoutExchange - netTotalWithoutExchange) * 100.0) / 100.0;
        table.addCell(getCell(taxNumber.toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
        table.addCell(getCell(grossTotalWithoutExchange.toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.NORMAL));
        table.addCell(getCell(invoice.getCurrency().getCode(), PdfPCell.ALIGN_CENTER, true, 9, Font.ITALIC));
        Double subTaxWithCurrencyExchange = Math.round((item.getItemGrossTotalPrice() - item.getItemNetTotalPrice()) * 100.0) / 100.0;
        table.addCell(getCell(item.getItemNetTotalPrice().toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.ITALIC));
        table.addCell(getCell(subTaxWithCurrencyExchange.toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.ITALIC));
        table.addCell(getCell(item.getItemGrossTotalPrice().toString(), PdfPCell.ALIGN_CENTER, true, 9, Font.ITALIC));
    }

    private void addCustomCellPaddingBottom(PdfPTable table) {
        for (int i = 0; i < table.size(); i++) {
            for (PdfPCell cell : table.getRow(i).getCells()) {
                if(cell != null) {
                    cell.setPaddingBottom(5f);
                }
            }
        }
    }

    private Element additionalInfoInfo() {
        PdfPTable additionalInfo = new PdfPTable(1);
        if (invoice.getAdditionalInfo() != null) {
            additionalInfo.setWidthPercentage(33);
            additionalInfo.setSpacingAfter(10f);
            additionalInfo.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            additionalInfo.addCell(getCell(invoice.getAdditionalInfo(), PdfPCell.ALIGN_RIGHT, false, 9, Font.BOLD));
            return additionalInfo;
        }
        return additionalInfo;
    }

    private Element summaryInfo() throws DocumentException {
        PdfPTable summary = new PdfPTable(4);
        float[] colWidths = new float[] { 8.5f, 10.5f, 10.5f, 10.5f };
        summary.setWidthPercentage(colWidths, pdfDoc.getPageSize());

        summary.setWidthPercentage(38.5f);
        summary.setSpacingAfter(10f);
        summary.setHorizontalAlignment(PdfPTable.ALIGN_RIGHT);
        summary.addCell(getCell("Summary:", PdfPTable.ALIGN_CENTER, true, 9, Font.BOLD));
        summary.addCell(getCell(invoice.getNetTotalPrice().toString(), PdfPTable.ALIGN_CENTER, true, 9, Font.BOLD));
        Double taxNumber = Math.round((invoice.getGrossTotalPrice() - invoice.getNetTotalPrice()) * 100.0) / 100.0;
        summary.addCell(getCell(taxNumber.toString(), PdfPTable.ALIGN_CENTER, true, 9, Font.BOLD));
        summary.addCell(getCell(invoice.getGrossTotalPrice().toString(), PdfPTable.ALIGN_CENTER, true, 9, Font.BOLD));
        addCustomCellPaddingBottom(summary);
        return summary;
    }

    private Element totalInfo() {
        PdfPTable total = new PdfPTable(1);
        total.setWidthPercentage(27);
        total.setSpacingAfter(10f);
        total.setHorizontalAlignment(PdfPTable.ALIGN_RIGHT);
        total.addCell(getCell("Total: " + invoice.getGrossTotalPrice() + " " + invoice.getCurrency().getCode(), PdfPCell.ALIGN_RIGHT, false, 10, Font.BOLD));
        return total;
    }

    private Element paymentInfo() {
        PdfPTable payment = new PdfPTable(1);
        payment.setWidthPercentage(33);
        payment.setSpacingAfter(20f);
        payment.setHorizontalAlignment(PdfPTable.ALIGN_RIGHT);
        payment.addCell(getCell("Payment method: " + invoice.getPaymentMethod().toString(), PdfPCell.ALIGN_RIGHT, false, 9, Font.NORMAL));
        payment.addCell(getCell("Payment date: " + dateFormat.format(invoice.getPaymentDate()), PdfPCell.ALIGN_RIGHT, false, 9, Font.NORMAL));
        return payment;
    }

    private Element currencyExchangeInfo() {
        PdfPTable exchange = new PdfPTable(1);
        if (invoice.getExchangeDate() != null && invoice.getExchangeRateNumber() != null) {
            exchange.setWidthPercentage(33);
            exchange.setSpacingAfter(20f);
            exchange.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            Currency invoiceItemCurrency = currencyDao.load(invoice.getItems().get(0).getCurrencyId());
            String otherCurrency = (!invoice.getCurrency().getCode().equals("PLN")) ? invoice.getCurrency().getCode() :
                    invoiceItemCurrency.getCode();
            exchange.addCell(getCell("Exchange date: " + dateFormat.format(invoice.getExchangeDate()) + "\n"
                    + "Exchange rate: 1 " + otherCurrency + " = " + invoice.getExchangeRate() + " PLN \n"
                    + "Exchange rate number: " + invoice.getExchangeRateNumber(), PdfPCell.ALIGN_RIGHT, false, 9, Font.NORMAL));
        }
        return exchange;
    }

    private void writeBankInfo() throws DocumentException {
        PdfPTable bank = new PdfPTable(1);
        bank.setWidthPercentage(100);
        bank.setSpacingAfter(10f);
        bank.addCell(getCell("Bank account number: " + invoice.getCompanyFrom().getBankAccountNumber(), PdfPCell.ALIGN_LEFT, false, 9, Font.NORMAL));
        bank.setTotalWidth(pdfDoc.getPageSize().getWidth() - pdfDoc.leftMargin() - pdfDoc.rightMargin());
        if (isElementHigherThanFreeSpace(bank.getTotalHeight() + 80))
        {
            addNewPage();
            pdfDoc.add(bank);
        } else
        {
            bank.writeSelectedRows(0, -1, pdfDoc.leftMargin(), bank.getTotalHeight() + 80, writer.getDirectContent());
        }
    }

    private boolean isElementHigherThanFreeSpace(float elementYCooridnateToWriteFromBottom) {
        return elementYCooridnateToWriteFromBottom > writer.getVerticalPosition(true) ? true : false;
    }

    private void addNewPage() {
        pdfDoc.newPage();
    }

    private void writeRepresentativeInfo() throws DocumentException {
        PdfPTable representative = new PdfPTable(2);
        representative.setWidthPercentage(100);
        representative.setSpacingAfter(10f);
        representative.addCell(getCell("", PdfPCell.ALIGN_LEFT, false, 9, Font.NORMAL));
        representative.addCell(getCell("", PdfPCell.ALIGN_CENTER, false, 9, Font.NORMAL));
        representative.addCell(getCell("Full name of person authorized for document collection", PdfPCell.ALIGN_CENTER, false, 9, Font.NORMAL));
        representative.addCell(getCell("Full name of person authorized for document issuance", PdfPCell.ALIGN_CENTER, false, 9, Font.NORMAL));
        representative.setTotalWidth(pdfDoc.getPageSize().getWidth() - pdfDoc.leftMargin() - pdfDoc.rightMargin());
        if (writer.getPageNumber() > 1)
        {
            pdfDoc.add(representative);
        }
        else
        {
            representative.writeSelectedRows(0, -1, pdfDoc.leftMargin(), representative.getTotalHeight() + 30, writer.getDirectContent());
        }
    }
}
