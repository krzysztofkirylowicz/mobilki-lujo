package wooocash.pl.mobileinvoice.util;

import java.util.ArrayList;
import java.util.List;

import wooocash.pl.mobileinvoice.domain.Company;
import wooocash.pl.mobileinvoice.greendao.CompanyDao;

public class CompanyService {

    private ValidationService validationService;
    private CompanyDao companyDao;

    public CompanyService(CompanyDao companyDao) {
        this.validationService = new ValidationService();
        this.companyDao = companyDao;
    }

    public boolean isTaxNumberValid(String taxNumber) {
        return taxNumber.length() == 10 ? true : false;
    }

    public boolean isBankAccountNumberValid(String bankAccountNumber) {
        return bankAccountNumber.length() == 26 ? true : false;
    }

    public boolean isTaxRateNumberUnique(String taxNumber) {
        Company company = companyDao.queryBuilder().where(CompanyDao.Properties.TaxNumber.eq(taxNumber)).unique();
        return company == null ? true : false;
    }

    public Company findCompanyById(long companyID) {
        Company company = companyDao.queryBuilder().where(CompanyDao.Properties.Id.eq(companyID)).unique();
        return company;
    }

    public Company findCompanyByTaxNumber(String taxNumber) {
        Company company = companyDao.queryBuilder().where(CompanyDao.Properties.TaxNumber.eq(taxNumber)).unique();
        return company;
    }

    public List<Company> findMyCompanies() {
        List<Company> myCompanies = companyDao.queryBuilder()
                .where(CompanyDao.Properties.CanCreateInvoices.eq(true))
                .orderDesc(CompanyDao.Properties.Id).list();
        return myCompanies;
    }

    public List<Company> findContractors() {
        List<Company> contractors = companyDao.queryBuilder()
                .where(CompanyDao.Properties.CanCreateInvoices.eq(false))
                .orderDesc(CompanyDao.Properties.Id).list();
        return contractors;
    }

    public List<String> getMyCompaniesNamesWithId() {
        List<String> myCompaniesNamesWithId = new ArrayList<>();

        for (Company c : findMyCompanies()) {
            myCompaniesNamesWithId.add(c.getId() + ". " + c.getName());
        }

        return myCompaniesNamesWithId;
    }
}
