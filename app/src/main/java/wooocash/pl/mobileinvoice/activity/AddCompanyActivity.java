package wooocash.pl.mobileinvoice.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Address;
import wooocash.pl.mobileinvoice.domain.Company;
import wooocash.pl.mobileinvoice.greendao.AddressDao;
import wooocash.pl.mobileinvoice.greendao.CompanyDao;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.main.App;
import wooocash.pl.mobileinvoice.util.CompanyService;
import wooocash.pl.mobileinvoice.util.ValidationService;

public class AddCompanyActivity extends AppCompatActivity {

    private AddressDao addressDao;
    private CompanyDao companyDao;
    private ValidationService validationService;
    private CompanyService companyService;

    @BindView(R.id.companyNameInput)
    EditText companyNameInput;

    @BindView(R.id.taxtNumberInput)
    EditText taxNumberInput;

    @BindView(R.id.bankAccountNumberInput)
    EditText bankAccountNumberInput;

    @BindView(R.id.addressInput)
    EditText addressInput;

    @BindView(R.id.postCodeInput)
    EditText postCodeInput;

    @BindView(R.id.cityInput)
    EditText cityInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_company);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Adding a company");

        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        this.addressDao = daoSession.getAddressDao();
        this.companyDao = daoSession.getCompanyDao();
        this.validationService = new ValidationService();
        this.companyService = new CompanyService(companyDao);
    }

    @OnClick(R.id.createCompanyButton)
    public void createCompany() {
        if (validationService.isRequiredInputsFilled(companyNameInput, taxNumberInput, bankAccountNumberInput,
                addressInput, postCodeInput, cityInput)) {

            if (!companyService.isTaxNumberValid(taxNumberInput.getText().toString())) {
                taxNumberInput.setError("Tax number must have 10 characters!");
                return;
            }

            if (!companyService.isTaxRateNumberUnique(taxNumberInput.getText().toString())) {
                taxNumberInput.setError("Tax number must be unique!");
                return;
            }

            if (!companyService.isBankAccountNumberValid(bankAccountNumberInput.getText().toString())) {
                bankAccountNumberInput.setError("Bank account number must have 26 characters!");
                return;
            }

            Address address = new Address();
            address.setAddress(addressInput.getText().toString());
            address.setPostCode(postCodeInput.getText().toString());
            address.setCity(cityInput.getText().toString());
            addressDao.insert(address);

            Company company = new Company();
            company.setCanCreateInvoices(true);
            company.setName(companyNameInput.getText().toString());
            company.setTaxNumber(taxNumberInput.getText().toString());
            company.setBankAccountNumber(bankAccountNumberInput.getText().toString());
            company.setAddressId(address.getId());
            companyDao.insert(company);

            Intent intent = new Intent(this, MyCompaniesActivity.class);
            intent.putExtra("TOAST_MESSAGE", "The company has been added!");
            startActivity(intent);
        }
    }
}
