package wooocash.pl.mobileinvoice.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Currency;
import wooocash.pl.mobileinvoice.domain.Invoice;
import wooocash.pl.mobileinvoice.domain.InvoiceItem;
import wooocash.pl.mobileinvoice.greendao.CurrencyDao;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.greendao.InvoiceDao;
import wooocash.pl.mobileinvoice.greendao.InvoiceItemDao;
import wooocash.pl.mobileinvoice.main.App;
import wooocash.pl.mobileinvoice.util.InvoicePDFGenerator;
import wooocash.pl.mobileinvoice.util.InvoiceService;
import wooocash.pl.mobileinvoice.util.ValidationService;

public class InvoiceDetailsActivity extends AppCompatActivity {

    private InvoiceDao invoiceDao;
    private InvoiceItemDao invoiceItemDao;
    private CurrencyDao currencyDao;
    private InvoiceService invoiceService;
    private ValidationService validationService;
    private SimpleDateFormat dateFormat;

    @BindView(R.id.invoiceDateTextView)
    TextView invoiceDateTextView;

    @BindView(R.id.invoiceSaleDateTextView)
    TextView invoiceSaleDateTextView;

    @BindView(R.id.companyTaxNumberTextView)
    TextView companyTaxNumberTextView;

    @BindView(R.id.companyNameTextView)
    TextView companyNameTextView;

    @BindView(R.id.companyAddressTextView)
    TextView companyAddressTextView;

    @BindView(R.id.companyPostCodeTextView)
    TextView companyPostCodeTextView;

    @BindView(R.id.companyCityTextView)
    TextView companyCityTextView;

    @BindView(R.id.contractorTaxNumberTextView)
    TextView contractorTaxNumberTextView;

    @BindView(R.id.contractorNameTextView)
    TextView contractorNameTextView;

    @BindView(R.id.contractorAddressTextView)
    TextView contractorAddressTextView;

    @BindView(R.id.contractorCityTextView)
    TextView contractorCityTextView;

    @BindView(R.id.contractorPostCodeTextView)
    TextView contractorPostCodeTextView;

    @BindView(R.id.invoiceNumberTextView)
    TextView invoiceNumberTextView;

    @BindView(R.id.invoiceItemDescriptionTextView)
    TextView invoiceItemDescriptionTextView;

    @BindView(R.id.invoiceItemPriceTextView)
    TextView invoiceItemPriceTextView;

    @BindView(R.id.invoiceItemCountTextView)
    TextView invoiceItemCountTextView;

    @BindView(R.id.invoiceItemTaxRateTextView)
    TextView invoiceItemTaxRateTextView;

    @BindView(R.id.invoiceNetTotalTextView)
    TextView invoiceNetTotalTextView;

    @BindView(R.id.invoiceGrossTotalTextView)
    TextView invoiceGrossTotalTextView;

    @BindView(R.id.invoicePaymentMethodTextView)
    TextView invoicePaymentMethodTextView;

    @BindView(R.id.invoicePaymentDateTextView)
    TextView invoicePaymentDateTextView;

    @BindView(R.id.invoiceAdditionalInfoTextView)
    TextView invoiceAdditionalInfoTextView;

    @BindView(R.id.invoiceExchangeDateTextView)
    TextView invoiceExchangeDateTextView;

    @BindView(R.id.invoiceExchangeRateTextView)
    TextView invoiceExchangeRateTextView;

    @BindView(R.id.invoiceExchangeRateNumberTextView)
    TextView invoiceExchangeRateNumberTextView;

    @BindView(R.id.invoiceAdditionalInfoLabel)
    TextView invoiceAdditionalInfoLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_details);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Invoice details");

        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        this.invoiceDao = daoSession.getInvoiceDao();
        this.invoiceItemDao = daoSession.getInvoiceItemDao();
        this.currencyDao = daoSession.getCurrencyDao();
        this.invoiceService = new InvoiceService(invoiceDao);
        this.validationService = new ValidationService();
        this.dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        showInvoiceDetails(getInvoiceIdFromIntent());
    }

    private long getInvoiceIdFromIntent() {
        Intent intent = getIntent();
        long invoiceID = intent.getLongExtra("INVOICE_ID", 0);
        return invoiceID;
    }

    private void showInvoiceDetails(long invoiceID) {
        Invoice invoice = invoiceService.findInvoiceById(invoiceID);

        if (invoice != null) {
            invoiceDateTextView.setText(dateFormat.format(invoice.getInvoiceDate()));
            invoiceSaleDateTextView.setText(dateFormat.format(invoice.getSaleDate()));
            companyTaxNumberTextView.setText("(" + invoice.getCompanyFrom().getTaxNumber() + ")");
            companyNameTextView.setText(invoice.getCompanyFrom().getName());
            companyAddressTextView.setText("ul. " + invoice.getCompanyFrom().getAddress().getAddress());
            companyPostCodeTextView.setText(invoice.getCompanyFrom().getAddress().getPostCode() + ",");
            companyCityTextView.setText(invoice.getCompanyFrom().getAddress().getCity());
            contractorTaxNumberTextView.setText("(" + invoice.getCompanyTo().getTaxNumber() + ")");
            contractorNameTextView.setText(invoice.getCompanyTo().getName());
            contractorAddressTextView.setText("ul. " + invoice.getCompanyTo().getAddress().getAddress());
            contractorPostCodeTextView.setText(invoice.getCompanyTo().getAddress().getPostCode() + ",");
            contractorCityTextView.setText(invoice.getCompanyTo().getAddress().getCity());
            invoiceNumberTextView.setText(invoice.getInvoiceNumber());
            InvoiceItem invoiceItem = invoice.getItems().get(0);
            invoiceItemDescriptionTextView.setText(invoiceItem.getDescription());
            Currency invoiceItemCurrency = currencyDao.load(invoiceItem.getCurrencyId());
            invoiceItemPriceTextView.setText(invoiceItem.getSingleUnitPrice().toString() + " " + invoiceItemCurrency.getCode());
            invoiceItemCountTextView.setText(invoiceItem.getCount().toString() + " " + invoiceItem.getUnit().toString());
            invoiceItemTaxRateTextView.setText(invoiceItem.getTaxRate().toString() + "%");
            invoiceNetTotalTextView.setText(invoice.getNetTotalPrice().toString() + " " + invoice.getCurrency().getCode());
            invoiceGrossTotalTextView.setText(invoice.getGrossTotalPrice().toString() + " " + invoice.getCurrency().getCode());
            invoicePaymentMethodTextView.setText(invoice.getPaymentMethod().toString());
            invoicePaymentDateTextView.setText(dateFormat.format(invoice.getPaymentDate()));
            if (!validationService.isStringNullOrEmpty(invoice.getAdditionalInfo())) {
                invoiceAdditionalInfoLabel.setVisibility(View.VISIBLE);
                invoiceAdditionalInfoTextView.setVisibility(View.VISIBLE);
                invoiceAdditionalInfoTextView.setText(invoice.getAdditionalInfo());
            }
            if (invoice.getExchangeDate() != null && invoice.getExchangeRate() != null && invoice.getExchangeRateNumber() != null) {
                invoiceExchangeDateTextView.setText(dateFormat.format(invoice.getExchangeDate()));
                invoiceExchangeRateTextView.setText(invoice.getExchangeRate().toString());
                invoiceExchangeRateNumberTextView.setText(invoice.getExchangeRateNumber());
                invoiceExchangeDateTextView.setVisibility(View.VISIBLE);
                invoiceExchangeRateTextView.setVisibility(View.VISIBLE);
                invoiceExchangeRateNumberTextView.setVisibility(View.VISIBLE);
            }
        } else {
            goToInvoicesView("Selected invoice no longer exists in the database!");
        }
    }

    private void goToInvoicesView(String toastMessage) {
        Intent goToInvoices = new Intent(this, MenuActivity.class);
        goToInvoices.putExtra("TOAST_MESSAGE", toastMessage);
        startActivity(goToInvoices);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_delete_generate_invoice_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.editOption) {
            goToEditInvoiceView();
        } else if (id == R.id.deleteOption) {
            deleteInvoiceWithInvoiceItem();
        } else if (id == R.id.generateOption) {
            InvoicePDFGenerator invoicePDFGenerator = new InvoicePDFGenerator(currencyDao, invoiceService);
            try {
                invoicePDFGenerator.generate(invoiceDao.load(getInvoiceIdFromIntent()));
                goToInvoicesView("The document has been generated.");
            } catch (Exception e) {
                e.printStackTrace(System.out);
                Toast toast = Toast.makeText(getApplicationContext(), "Exception occurred while generating invoice .pdf document!",
                        Toast.LENGTH_LONG);
                toast.show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void goToEditInvoiceView() {
        Intent goToEditInvoiceView = new Intent(this, EditInvoiceActivity.class);
        goToEditInvoiceView.putExtra("INVOICE_ID", getInvoiceIdFromIntent());
        startActivity(goToEditInvoiceView);
    }

    private void deleteInvoiceWithInvoiceItem() {
        Invoice invoice = invoiceDao.load(getInvoiceIdFromIntent());

        if (invoice != null) {
            InvoiceItem invoiceItem = invoice.getItems().get(0);
            invoiceItemDao.delete(invoiceItem);
            invoiceDao.delete(invoice);
            goToInvoicesView("The invoice has been deleted!");
        } else {
            goToInvoicesView("Selected invoice no longer exists in the database!");
        }
    }
}
