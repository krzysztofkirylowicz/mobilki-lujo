package wooocash.pl.mobileinvoice.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Address;
import wooocash.pl.mobileinvoice.domain.Company;
import wooocash.pl.mobileinvoice.domain.Invoice;
import wooocash.pl.mobileinvoice.greendao.AddressDao;
import wooocash.pl.mobileinvoice.greendao.CompanyDao;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.greendao.InvoiceDao;
import wooocash.pl.mobileinvoice.main.App;
import wooocash.pl.mobileinvoice.util.CompanyService;

public class CompanyDetailsActivity extends AppCompatActivity {

    private CompanyDao companyDao;
    private AddressDao addressDao;
    private InvoiceDao invoiceDao;
    private CompanyService companyService;

    @BindView(R.id.companyIdTextView)
    TextView companyIdTextView;

    @BindView(R.id.companyNameTextView)
    TextView companyNameTextView;

    @BindView(R.id.companyTaxNumberTextView)
    TextView companyTaxNumberTextView;

    @BindView(R.id.companyBankAccountNumberTextView)
    TextView companyBankAccountNumberTextView;

    @BindView(R.id.companyStreetTextView)
    TextView companyStreetTextView;

    @BindView(R.id.companyPostCodeTextView)
    TextView companyPostCodeTextView;

    @BindView(R.id.companyCityTextView)
    TextView companyCityTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_details);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Company details");

        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        this.companyDao = daoSession.getCompanyDao();
        this.addressDao = daoSession.getAddressDao();
        this.invoiceDao = daoSession.getInvoiceDao();
        this.companyService = new CompanyService(companyDao);

        showCompanyDetails(getCompanyIdFromIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_or_delete_entity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.editOption) {
            goToEditCompanyView();
        } else if (id == R.id.deleteOption) {
            deleteCompanyWithAddress();
        }

        return super.onOptionsItemSelected(item);
    }

    private void goToEditCompanyView() {
        Intent goToEditCopmanyView = new Intent(this, EditCompanyActivity.class);
        goToEditCopmanyView.putExtra("COMPANY_ID", getCompanyIdFromIntent());
        startActivity(goToEditCopmanyView);
    }

    private void deleteCompanyWithAddress() {
        Company company = companyService.findCompanyById(getCompanyIdFromIntent());

        if (company != null) {
            if (!hasCompanyAssignedInvoices(company.getId())) {
                Address address = company.getAddress();
                companyDao.delete(company);
                addressDao.delete(address);
                goToMyCompaniesView("The company has been deleted!");
            } else {
                goToMyCompaniesView("You cannot delete, because selected company has assigned invoices!");
            }
        } else {
            goToMyCompaniesView("Selected company no longer exists in the database!");
        }
    }

    private boolean hasCompanyAssignedInvoices(long companyID) {
        QueryBuilder<Invoice> qb = invoiceDao.queryBuilder().limit(1);
        Invoice invoice = qb.whereOr(InvoiceDao.Properties.CompanyToId.eq(companyID),
                InvoiceDao.Properties.CompanyFromId.eq(companyID)).unique();
        return invoice != null ? true : false;
    }

    private void goToMyCompaniesView(String toastMessage) {
        Intent goToMyCompanies = new Intent(this, MyCompaniesActivity.class);
        goToMyCompanies.putExtra("TOAST_MESSAGE", toastMessage);
        startActivity(goToMyCompanies);
    }

    private long getCompanyIdFromIntent() {
        Intent intent = getIntent();
        long companyID = intent.getLongExtra("COMPANY_ID", 0);
        return companyID;
    }

    private void showCompanyDetails(long companyID) {
        Company company = companyService.findCompanyById(companyID);

        if (company != null) {
            companyIdTextView.setText(company.getId().toString());
            companyNameTextView.setText(company.getName());
            companyTaxNumberTextView.setText(company.getTaxNumber());
            companyBankAccountNumberTextView.setText(company.getBankAccountNumber());
            companyStreetTextView.setText(company.getAddress().getAddress());
            companyPostCodeTextView.setText(company.getAddress().getPostCode());
            companyCityTextView.setText(company.getAddress().getCity());
        } else {
            goToMyCompaniesView("Selected company no longer exists in the database!");
        }
    }
}
