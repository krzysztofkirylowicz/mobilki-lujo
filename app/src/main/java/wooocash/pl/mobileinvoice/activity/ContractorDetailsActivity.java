package wooocash.pl.mobileinvoice.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.greenrobot.greendao.query.QueryBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;
import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Address;
import wooocash.pl.mobileinvoice.domain.Company;
import wooocash.pl.mobileinvoice.domain.Invoice;
import wooocash.pl.mobileinvoice.greendao.AddressDao;
import wooocash.pl.mobileinvoice.greendao.CompanyDao;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.greendao.InvoiceDao;
import wooocash.pl.mobileinvoice.main.App;
import wooocash.pl.mobileinvoice.util.CompanyService;

public class ContractorDetailsActivity extends AppCompatActivity {

    private CompanyDao companyDao;
    private AddressDao addressDao;
    private InvoiceDao invoiceDao;
    private CompanyService companyService;

    @BindView(R.id.contractorIdTextView)
    TextView contractorIdTextView;

    @BindView(R.id.contractorNameTextView)
    TextView contractorNameTextView;

    @BindView(R.id.contractorTaxNumberTextView)
    TextView contractorTaxNumberTextView;

    @BindView(R.id.contractorStreetTextView)
    TextView contractorStreetTextView;

    @BindView(R.id.contractorPostCodeTextView)
    TextView contractorPostCodeTextView;

    @BindView(R.id.contractorCityTextView)
    TextView contractorCityTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contractor_details);

        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Contractor details");

        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        this.companyDao = daoSession.getCompanyDao();
        this.addressDao = daoSession.getAddressDao();
        this.invoiceDao = daoSession.getInvoiceDao();
        this.companyService = new CompanyService(companyDao);

        showContractorDetails(getContractorIdFromIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_or_delete_entity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.editOption) {
            goToEditContractorView();
        } else if (id == R.id.deleteOption) {
            deleteContractorWithAddress();
        }

        return super.onOptionsItemSelected(item);
    }

    private void goToEditContractorView() {
        Intent goToEditContractorView = new Intent(this, EditContractorActivity.class);
        goToEditContractorView.putExtra("CONTRACTOR_ID", getContractorIdFromIntent());
        startActivity(goToEditContractorView);
    }

    private void deleteContractorWithAddress() {
        Company contractor = companyService.findCompanyById(getContractorIdFromIntent());

        if (contractor != null) {
            if (!hasContractorAssignedInvoices(contractor.getId())) {
                Address address = contractor.getAddress();
                companyDao.delete(contractor);
                addressDao.delete(address);
                goToContractorsView("The contractor has been deleted!");
            } else {
                goToContractorsView("You cannot delete, because selected contractor has assigned invoices!");
            }
        } else {
            goToContractorsView("Selected contractor no longer exists in the database!");
        }
    }

    private boolean hasContractorAssignedInvoices(long contractorID) {
        QueryBuilder<Invoice> qb = invoiceDao.queryBuilder().limit(1);
        Invoice invoice = qb.where(InvoiceDao.Properties.CompanyToId.eq(contractorID)).unique();
        return invoice != null ? true : false;
    }

    private void goToContractorsView(String toastMessage) {
        Intent goToContractors = new Intent(this, ContractorsActivity.class);
        goToContractors.putExtra("TOAST_MESSAGE", toastMessage);
        startActivity(goToContractors);
    }

    private long getContractorIdFromIntent() {
        Intent intent = getIntent();
        long contractorID = intent.getLongExtra("CONTRACTOR_ID", 0);
        return contractorID;
    }

    private void showContractorDetails(long companyID) {
        Company contractor = companyService.findCompanyById(companyID);

        if (contractor != null) {
            contractorIdTextView.setText(contractor.getId().toString());
            contractorNameTextView.setText(contractor.getName());
            contractorTaxNumberTextView.setText(contractor.getTaxNumber());
            contractorStreetTextView.setText(contractor.getAddress().getAddress());
            contractorPostCodeTextView.setText(contractor.getAddress().getPostCode());
            contractorCityTextView.setText(contractor.getAddress().getCity());
        } else {
            goToContractorsView("Selected contractor no longer exists in the database!");
        }
    }
}
