package wooocash.pl.mobileinvoice.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Address;
import wooocash.pl.mobileinvoice.domain.Company;
import wooocash.pl.mobileinvoice.domain.Currency;
import wooocash.pl.mobileinvoice.domain.Invoice;
import wooocash.pl.mobileinvoice.domain.InvoiceItem;
import wooocash.pl.mobileinvoice.greendao.AddressDao;
import wooocash.pl.mobileinvoice.greendao.CompanyDao;
import wooocash.pl.mobileinvoice.greendao.CurrencyDao;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.greendao.InvoiceDao;
import wooocash.pl.mobileinvoice.greendao.InvoiceItemDao;
import wooocash.pl.mobileinvoice.main.App;
import wooocash.pl.mobileinvoice.util.CompanyService;
import wooocash.pl.mobileinvoice.util.CurrencyService;
import wooocash.pl.mobileinvoice.util.InvoiceService;
import wooocash.pl.mobileinvoice.util.ValidationService;

public class EditInvoiceActivity extends AppCompatActivity {

    private ValidationService validationService;
    private CompanyService companyService;
    private InvoiceService invoiceService;
    private CurrencyService currencyService;
    private CompanyDao companyDao;
    private InvoiceDao invoiceDao;
    private InvoiceItemDao invoiceItemDao;
    private CurrencyDao currencyDao;
    private AddressDao addressDao;
    private Calendar saleDateCalendar = Calendar.getInstance();
    private Calendar paymentDateCalendar = Calendar.getInstance();
    private Calendar exchangeDateCalendar = Calendar.getInstance();
    private SimpleDateFormat dateFormat;
    private Invoice invoice;
    private boolean initOnLoadInvoiceDataSelectInvoiceItemCurrency = true;

    @BindView(R.id.selectMyCompanySpinner)
    Spinner selectMyCompanySpinner;

    @BindView(R.id.selectPaymentMethodSpinner)
    Spinner selectPaymentMethodSpinner;

    @BindView(R.id.selectInvoiceCurrencySpinner)
    Spinner selectInvoiceCurrencySpinner;

    @BindView(R.id.selectItemCurrencySpinner0)
    Spinner selectItemCurrencySpinner0;

    @BindView(R.id.selectUnitTypeSpinner0)
    Spinner selectUnitTypeSpinner0;

    @BindView(R.id.selectedMyCompanyTaxNumberTextView)
    TextView selectedMyCompanyTaxNumberTextView;

    @BindView(R.id.taxNumberOfTheBuyerInput)
    EditText taxNumberOfTheBuyerInput;

    @BindView(R.id.contractorNameInput)
    EditText contractorNameInput;

    @BindView(R.id.contractorAddressInput)
    EditText contractorAddressInput;

    @BindView(R.id.contractorPostCodeInput)
    EditText contractorPostCodeInput;

    @BindView(R.id.contractorCityInput)
    EditText contractorCityInput;

    @BindView(R.id.saleDateInput)
    EditText saleDateInput;

    @BindView(R.id.paymentDateInput)
    EditText paymentDateInput;

    @BindView(R.id.exchangeDateInput)
    EditText exchangeDateInput;

    @BindView(R.id.exchangeRateTextView)
    TextView exchangeRateTextView;

    @BindView(R.id.exchangeRateNumberTextView)
    TextView exchangeRateNumberTextView;

    @BindView(R.id.additionalInfoInput)
    EditText additionalInfoInput;

    @BindView(R.id.descriptionInput0)
    EditText descriptionInput0;

    @BindView(R.id.singleUnitPriceInput0)
    EditText singleUnitPriceInput0;

    @BindView(R.id.taxRateInput0)
    EditText taxRateInput0;

    @BindView(R.id.countInput0)
    EditText countInput0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_invoice);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Editing an invoice");

        this.validationService = new ValidationService();
        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        this.companyDao = daoSession.getCompanyDao();
        this.invoiceDao = daoSession.getInvoiceDao();
        this.invoiceItemDao = daoSession.getInvoiceItemDao();
        this.currencyDao = daoSession.getCurrencyDao();
        this.addressDao = daoSession.getAddressDao();
        this.companyService = new CompanyService(companyDao);
        this.invoiceService = new InvoiceService(invoiceDao);
        this.currencyService = new CurrencyService(currencyDao);
        this.dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        this.invoice = invoiceService.findInvoiceById(getInvoiceIdFromIntent());

        setSelectMyCompanySpinnerItems();
        setOnItemSelectedListenerForSelectMyCompanySpinner();
        setSelectPaymentMethodSpinnerItems();
        setOnItemSelectedListenerForSelectPaymentMethodSpinner();
        setSelectInvoiceCurrencySpinnerItems();
        setOnItemSelectedListenerForSelectInvoiceCurrencySpinner();
        setSelectUnitTypeSpinnerItems();
        setOnItemSelectedListenerForSelectInvoiceItemCurrencySpinner();
        setUpInputsByValuesFromDB();
    }

    private long getInvoiceIdFromIntent() {
        Intent intent = getIntent();
        long invoiceID = intent.getLongExtra("INVOICE_ID", 0);
        return invoiceID;
    }

    private void setUpInputsByValuesFromDB() {
        selectMyCompanySpinner.setSelection(getItemIndexInSelect(selectMyCompanySpinner,
                invoice.getCompanyFrom().getId() + ". " + invoice.getCompanyFrom().getName()));
        selectPaymentMethodSpinner.setSelection(getItemIndexInSelect(selectPaymentMethodSpinner,
                invoice.getPaymentMethod().toString()));
        selectInvoiceCurrencySpinner.setSelection(getItemIndexInSelect(selectInvoiceCurrencySpinner,
                invoice.getCurrency().getCode()));
        taxNumberOfTheBuyerInput.setText(invoice.getCompanyTo().getTaxNumber());
        saleDateInput.setText(dateFormat.format(invoice.getSaleDate()));
        paymentDateInput.setText(dateFormat.format(invoice.getPaymentDate()));
        if (invoice.getExchangeDate() != null && invoice.getExchangeRate() != null && invoice.getExchangeRateNumber() != null) {
            exchangeDateInput.setText(dateFormat.format(invoice.getExchangeDate()));
            exchangeRateTextView.setText(invoice.getExchangeRate().toString());
            exchangeRateNumberTextView.setText(invoice.getExchangeRateNumber());
            invoiceService.showExchangeRateForm(exchangeDateInput, exchangeRateTextView, exchangeRateNumberTextView);
        }
        if (invoice.getAdditionalInfo() != null) {
            additionalInfoInput.setText(invoice.getAdditionalInfo());
        }
        InvoiceItem invoiceItem = invoice.getItems().get(0);
        selectUnitTypeSpinner0.setSelection(getItemIndexInSelect(selectUnitTypeSpinner0, invoiceItem.getUnit().toString()));
        descriptionInput0.setText(invoiceItem.getDescription());
        singleUnitPriceInput0.setText(invoiceItem.getSingleUnitPrice().toString());
        taxRateInput0.setText(invoiceItem.getTaxRate().toString());
        countInput0.setText(invoiceItem.getCount().toString());
        searchBuyerBasedOnTaxNumber();
    }

    private int getItemIndexInSelect(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }

        return index;
    }

    private void setSelectMyCompanySpinnerItems() {
        List<String> myCompaniesNamesWithId = companyService.getMyCompaniesNamesWithId();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, myCompaniesNamesWithId);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectMyCompanySpinner.setAdapter(dataAdapter);
    }

    private void setOnItemSelectedListenerForSelectMyCompanySpinner() {
        selectMyCompanySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String myCompanyNameWithId = parent.getItemAtPosition(position).toString();
                long selectedCompanyId = Long.parseLong(myCompanyNameWithId.split("\\.")[0]);
                selectedMyCompanyTaxNumberTextView.setText("Your company tax number: "
                        + companyService.findCompanyById(selectedCompanyId).getTaxNumber());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void setSelectPaymentMethodSpinnerItems() {
        ArrayAdapter<Invoice.PaymentMethod> dataAdapter = new ArrayAdapter<Invoice.PaymentMethod>(this, android.R.layout.simple_spinner_item, Invoice.PaymentMethod.values());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectPaymentMethodSpinner.setAdapter(dataAdapter);
    }

    private void setOnItemSelectedListenerForSelectPaymentMethodSpinner() {
        selectPaymentMethodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Invoice.PaymentMethod selectedPaymentMethod = (Invoice.PaymentMethod)parent.getItemAtPosition(position);

                if (selectedPaymentMethod == Invoice.PaymentMethod.CASH) {
                    paymentDateInput.getText().clear();
                    paymentDateInput.setVisibility(View.GONE);
                } else {
                    paymentDateInput.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void setSelectInvoiceCurrencySpinnerItems() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, loadAllCurrenciesFromDB());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectInvoiceCurrencySpinner.setAdapter(dataAdapter);
    }

    private List<String> loadAllCurrenciesFromDB() {
        List<Currency> currencies = currencyDao.loadAll();
        List<String> currenciesCode = new ArrayList<>();

        for (Currency c : currencies) {
            currenciesCode.add(c.getCode());
        }

        return currenciesCode;
    }

    private void setOnItemSelectedListenerForSelectInvoiceCurrencySpinner() {
        final Activity activity = this;
        selectInvoiceCurrencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCurrencyCode = parent.getItemAtPosition(position).toString();

                if (selectedCurrencyCode.equals("PLN")) {
                    if (!initOnLoadInvoiceDataSelectInvoiceItemCurrency) {
                        invoiceService.goneExchangeRateForm(exchangeDateInput, exchangeRateTextView, exchangeRateNumberTextView);
                    }
                    setSelectInvoiceItemCurrencySpinnerItems(null);
                } else {
                    invoiceService.getExchangeDataFromRestService(activity, selectedCurrencyCode, exchangeDateInput.getText().toString());
                    invoiceService.showExchangeRateForm(exchangeDateInput, exchangeRateTextView, exchangeRateNumberTextView);
                    setSelectInvoiceItemCurrencySpinnerItems(selectedCurrencyCode);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void setSelectInvoiceItemCurrencySpinnerItems(String selectedInvoiceForeignCurrency) {

        ArrayAdapter<String> dataAdapter;

        if (selectedInvoiceForeignCurrency != null) {
            List<String> currencies = new ArrayList<>();
            currencies.add("PLN");
            currencies.add(selectedInvoiceForeignCurrency);
            dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, currencies);
        } else {
            dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, loadAllCurrenciesFromDB());
        }

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectItemCurrencySpinner0.setAdapter(dataAdapter);

        if (initOnLoadInvoiceDataSelectInvoiceItemCurrency) {
            Currency invoiceItemCurrency = currencyDao.load(invoice.getItems().get(0).getCurrencyId());
            selectItemCurrencySpinner0.setSelection(getItemIndexInSelect(selectItemCurrencySpinner0, invoiceItemCurrency.getCode()));
            initOnLoadInvoiceDataSelectInvoiceItemCurrency = false;
        }
    }

    private void setOnItemSelectedListenerForSelectInvoiceItemCurrencySpinner() {
        final Activity activity = this;
        selectItemCurrencySpinner0.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selectedInvoiceItemCurrencyCode = parent.getItemAtPosition(position).toString();
                    String selectedInvoiceCurrencyCode = selectInvoiceCurrencySpinner.getSelectedItem().toString();

                    if (selectedInvoiceCurrencyCode.equals("PLN") && !selectedInvoiceItemCurrencyCode.equals("PLN")) {
                        if (validationService.isStringNullOrEmpty(exchangeDateInput.getText().toString())) {
                            Toast toast = Toast.makeText(activity.getApplicationContext(), "Please, fill in exchange date to get exchange data.", Toast.LENGTH_LONG);
                            toast.show();
                            invoiceService.showExchangeRateForm(exchangeDateInput, exchangeRateTextView, exchangeRateNumberTextView);
                        } else {
                            invoiceService.getExchangeDataFromRestService(activity, selectedInvoiceItemCurrencyCode, exchangeDateInput.getText().toString());
                        }
                    } else if (selectedInvoiceCurrencyCode.equals("PLN") && selectedInvoiceItemCurrencyCode.equals("PLN")) {
                        invoiceService.goneExchangeRateForm(exchangeDateInput, exchangeRateTextView, exchangeRateNumberTextView);
                    }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void setSelectUnitTypeSpinnerItems() {
        ArrayAdapter<InvoiceItem.Unit> dataAdapter = new ArrayAdapter<InvoiceItem.Unit>(this, android.R.layout.simple_spinner_item, InvoiceItem.Unit.values());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectUnitTypeSpinner0.setAdapter(dataAdapter);
    }

    @OnClick(R.id.searchBuyerBasedOnTaxNumberButton)
    public void searchBuyerBasedOnTaxNumber() {
        if (validationService.isRequiredInputsFilled(taxNumberOfTheBuyerInput)) {
            String taxNumber = taxNumberOfTheBuyerInput.getText().toString();
            if (companyService.isTaxNumberValid(taxNumber)) {
                Company company = companyService.findCompanyByTaxNumber(taxNumber);
                if (company != null) {
                    Drawable doneIcon = ContextCompat.getDrawable(this, R.drawable.ic_done_black_24px);
                    doneIcon.setBounds(0, 0, doneIcon.getIntrinsicWidth(), doneIcon.getIntrinsicHeight());
                    taxNumberOfTheBuyerInput.setError("Buyer's name: " + company.getName() + "\nAddress: "
                            + company.getAddress().getAddress() + "\n"
                            + company.getAddress().getPostCode() + ", "
                            + company.getAddress().getCity(), doneIcon);
                    invoiceService.goneAddCompanyForm(contractorNameInput, contractorAddressInput,
                            contractorPostCodeInput, contractorCityInput);
                } else {
                    Drawable warningIcon = ContextCompat.getDrawable(this, R.drawable.ic_warning_black_24px);
                    warningIcon.setBounds(0, 0, warningIcon.getIntrinsicWidth(), warningIcon.getIntrinsicHeight());
                    taxNumberOfTheBuyerInput.setError("You need to add a new contractor!", warningIcon);
                    invoiceService.showAddCompanyForm(contractorNameInput, contractorAddressInput,
                            contractorPostCodeInput, contractorCityInput);
                }
            } else {
                taxNumberOfTheBuyerInput.setError("Tax number must have 10 characters!");
            }
        }
    }

    @OnClick(R.id.saleDateInput)
    public void setDatePickerDialogForSaleDate() {
        new DatePickerDialog(EditInvoiceActivity.this, onSaleDateListener, saleDateCalendar
                .get(Calendar.YEAR), saleDateCalendar.get(Calendar.MONTH),
                saleDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    DatePickerDialog.OnDateSetListener onSaleDateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            saleDateCalendar.set(Calendar.YEAR, year);
            saleDateCalendar.set(Calendar.MONTH, monthOfYear);
            saleDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateSaleDateInput();
        }

    };

    private void updateSaleDateInput() {
        saleDateInput.setText(dateFormat.format(saleDateCalendar.getTime()));
    }

    @OnClick(R.id.paymentDateInput)
    public void setDatePickerDialogForPaymentDate() {
        new DatePickerDialog(EditInvoiceActivity.this, onPaymentDateListener, paymentDateCalendar
                .get(Calendar.YEAR), paymentDateCalendar.get(Calendar.MONTH),
                paymentDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    DatePickerDialog.OnDateSetListener onPaymentDateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            paymentDateCalendar.set(Calendar.YEAR, year);
            paymentDateCalendar.set(Calendar.MONTH, monthOfYear);
            paymentDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updatePaymentDateInput();
        }

    };

    private void updatePaymentDateInput() {
        paymentDateInput.setText(dateFormat.format(paymentDateCalendar.getTime()));
    }

    @OnClick(R.id.exchangeDateInput)
    public void setDatePickerDialogForExchangeDate() {
        new DatePickerDialog(EditInvoiceActivity.this, onExchangeDateListener, exchangeDateCalendar
                .get(Calendar.YEAR), exchangeDateCalendar.get(Calendar.MONTH),
                exchangeDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    DatePickerDialog.OnDateSetListener onExchangeDateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            exchangeDateCalendar.set(Calendar.YEAR, year);
            exchangeDateCalendar.set(Calendar.MONTH, monthOfYear);
            exchangeDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateExchangeDateInput();
        }

    };

    private void updateExchangeDateInput() {
        String dateAfterFromat = dateFormat.format(exchangeDateCalendar.getTime());
        exchangeDateInput.setText(dateAfterFromat);
        if (selectInvoiceCurrencySpinner.getSelectedItem().toString().equals("PLN")) {
            invoiceService.getExchangeDataFromRestService(this, selectItemCurrencySpinner0.getSelectedItem().toString(),
                    dateAfterFromat);
        } else {
            invoiceService.getExchangeDataFromRestService(this, selectInvoiceCurrencySpinner.getSelectedItem().toString(),
                    dateAfterFromat);
        }
    }

    @OnClick(R.id.editInvoiceButton)
    public void editInvoice() {
        if (validationService.isRequiredInputsFilled(taxNumberOfTheBuyerInput, saleDateInput, descriptionInput0,
                singleUnitPriceInput0, taxRateInput0, countInput0)) {

            Company contractor = null;
            Address contractorAddress = null;
            Invoice invoice = invoiceService.findInvoiceById(getInvoiceIdFromIntent());
            boolean createNewContractor = false;
            boolean areThereErrors = false;

            String taxNumber = taxNumberOfTheBuyerInput.getText().toString();
            if (companyService.isTaxNumberValid(taxNumber)) {
                contractor = companyService.findCompanyByTaxNumber(taxNumber);
                if (contractor == null) {
                    if (validationService.isRequiredInputsFilled(contractorNameInput, contractorAddressInput,
                            contractorPostCodeInput, contractorCityInput)) {
                        contractor = new Company();
                        contractor.setName(contractorNameInput.getText().toString());
                        contractor.setTaxNumber(taxNumberOfTheBuyerInput.getText().toString());
                        contractor.setCanCreateInvoices(false);
                        contractorAddress = new Address();
                        contractorAddress.setAddress(contractorAddressInput.getText().toString());
                        contractorAddress.setPostCode(contractorPostCodeInput.getText().toString());
                        contractorAddress.setCity(contractorCityInput.getText().toString());
                        createNewContractor = true;
                    } else {
                        areThereErrors = true;
                    }
                }
            } else {
                areThereErrors = true;
                taxNumberOfTheBuyerInput.setError("Tax number must have 10 characters!");
            }

            Invoice.PaymentMethod selectedPaymentMethod = (Invoice.PaymentMethod)selectPaymentMethodSpinner.getSelectedItem();
            invoice.setPaymentMethod(selectedPaymentMethod);

            if (selectedPaymentMethod == Invoice.PaymentMethod.TRANSFER) {
                if (!validationService.isRequiredInputsFilled(paymentDateInput)) {
                    areThereErrors = true;
                } else {
                    try {
                        invoice.setPaymentDate(dateFormat.parse(paymentDateInput.getText().toString()));
                    } catch (ParseException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }

            String invoiceCurrency = selectInvoiceCurrencySpinner.getSelectedItem().toString();
            String invoiceItemCurrency = selectItemCurrencySpinner0.getSelectedItem().toString();

            if (!invoiceCurrency.equals("PLN") || !invoiceItemCurrency.equals("PLN")) {
                if (validationService.isRequiredInputsFilled(exchangeDateInput)) {
                    String exchangeRateString = exchangeRateTextView.getText().toString().split(":")[1].trim();
                    String exchangeRateNumberString = exchangeRateNumberTextView.getText().toString().split(":")[1].trim();
                    if (validationService.isStringNullOrEmpty(exchangeRateString) || validationService.isStringNullOrEmpty(exchangeRateNumberString)) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Please, choose the right date exchange to get exchange data.", Toast.LENGTH_LONG);
                        toast.show();
                        areThereErrors = true;
                    } else {
                        try {
                            invoice.setExchangeDate(dateFormat.parse(exchangeDateInput.getText().toString()));
                        } catch (ParseException e) {
                            System.out.println(e.getMessage());
                        }
                        invoice.setExchangeRate(Double.parseDouble(exchangeRateString));
                        invoice.setExchangeRateNumber(exchangeRateNumberString);
                    }
                } else {
                    areThereErrors = true;
                }
            }

            double taxRate = Double.parseDouble(taxRateInput0.getText().toString());
            if (taxRate < 0) {
                taxRateInput0.setError("Tax rate must be greater than 0.");
                areThereErrors = true;
            }

            if (areThereErrors) return;

            String myCompanyNameWithId = selectMyCompanySpinner.getSelectedItem().toString();
            long selectedMyCompanyId = Long.parseLong(myCompanyNameWithId.split("\\.")[0]);
            invoice.setCompanyFromId(selectedMyCompanyId);

            InvoiceItem invoiceItem = invoice.getItems().get(0);
            invoiceItem.setDescription(descriptionInput0.getText().toString());
            invoiceItem.setSingleUnitPrice(Math.round(Double.parseDouble(singleUnitPriceInput0.getText().toString()) * 100.0) / 100.0);
            invoiceItem.setCount(Double.parseDouble(countInput0.getText().toString()));
            invoiceItem.setUnit((InvoiceItem.Unit)selectUnitTypeSpinner0.getSelectedItem());
            invoiceItem.setTaxRate(taxRate);
            invoiceItem.setCurrencyId(currencyService.findCurrencyByCode(selectItemCurrencySpinner0.getSelectedItem().toString()).getId());

            Double calculatedExchangeRate = invoiceService.calculateExchangeRate(invoice.getExchangeRate(), selectInvoiceCurrencySpinner.getSelectedItem().toString(),
                    selectItemCurrencySpinner0.getSelectedItem().toString());
            invoiceItem.setItemNetTotalPrice(Math.round((invoiceItem.getCount() * invoiceItem.getSingleUnitPrice() * calculatedExchangeRate) * 100.0) / 100.0);
            double netTotalPriceWithTaxRate = (1 + (taxRate / 100)) * invoiceItem.getItemNetTotalPrice();
            invoiceItem.setItemGrossTotalPrice(Math.round(netTotalPriceWithTaxRate * 100.0) / 100.0);

            invoice.setCurrencyId(currencyService.findCurrencyByCode(selectInvoiceCurrencySpinner.getSelectedItem().toString()).getId());

            try {
                invoice.setSaleDate(dateFormat.parse(saleDateInput.getText().toString()));
            } catch (ParseException e) {
                System.out.println(e.getMessage());
            }
            invoice.setAdditionalInfo(additionalInfoInput.getText().toString());
            invoice.setNetTotalPrice(invoiceItem.getItemNetTotalPrice());
            invoice.setGrossTotalPrice(invoiceItem.getItemGrossTotalPrice());

            if (createNewContractor) {
                addressDao.insert(contractorAddress);
                contractor.setAddressId(contractorAddress.getId());
                companyDao.insert(contractor);
            }

            invoice.setCompanyTo(contractor);
            invoice.setCompanyToId(contractor.getId());
            invoiceDao.update(invoice);
            invoiceItemDao.update(invoiceItem);

            Intent intent = new Intent(this, MenuActivity.class);
            intent.putExtra("TOAST_MESSAGE", "The invoice has been edited!");
            startActivity(intent);
        }
    }
}
