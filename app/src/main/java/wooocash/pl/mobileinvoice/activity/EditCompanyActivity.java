package wooocash.pl.mobileinvoice.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Address;
import wooocash.pl.mobileinvoice.domain.Company;
import wooocash.pl.mobileinvoice.greendao.AddressDao;
import wooocash.pl.mobileinvoice.greendao.CompanyDao;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.main.App;
import wooocash.pl.mobileinvoice.util.CompanyService;
import wooocash.pl.mobileinvoice.util.ValidationService;

public class EditCompanyActivity extends AppCompatActivity {

    private AddressDao addressDao;
    private CompanyDao companyDao;
    private ValidationService validationService;
    private CompanyService companyService;
    private Company company;
    private String oldCompanyTaxNumber;

    @BindView(R.id.companyNameInput)
    EditText companyNameInput;

    @BindView(R.id.taxtNumberInput)
    EditText taxNumberInput;

    @BindView(R.id.bankAccountNumberInput)
    EditText bankAccountNumberInput;

    @BindView(R.id.addressInput)
    EditText addressInput;

    @BindView(R.id.postCodeInput)
    EditText postCodeInput;

    @BindView(R.id.cityInput)
    EditText cityInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_company);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Editing a company");

        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        this.addressDao = daoSession.getAddressDao();
        this.companyDao = daoSession.getCompanyDao();
        this.validationService = new ValidationService();
        this.companyService = new CompanyService(companyDao);
        this.company = companyService.findCompanyById(getCompanyIdFromIntent());
        this.oldCompanyTaxNumber = company.getTaxNumber();
        setUpInputsByValuesFromDB();
    }

    private long getCompanyIdFromIntent() {
        Intent intent = getIntent();
        long companyID = intent.getLongExtra("COMPANY_ID", 0);
        return companyID;
    }

    private void setUpInputsByValuesFromDB() {
        companyNameInput.setText(company.getName());
        taxNumberInput.setText(company.getTaxNumber());
        bankAccountNumberInput.setText(company.getBankAccountNumber());
        addressInput.setText(company.getAddress().getAddress());
        postCodeInput.setText(company.getAddress().getPostCode());
        cityInput.setText(company.getAddress().getCity());
    }

    @OnClick(R.id.editCompanyButton)
    public void editCompany() {
        if (validationService.isRequiredInputsFilled(companyNameInput, taxNumberInput, bankAccountNumberInput,
                addressInput, postCodeInput, cityInput)) {

            if (!companyService.isTaxNumberValid(taxNumberInput.getText().toString())) {
                taxNumberInput.setError("Tax number must have 10 characters!");
                return;
            }

            if (!oldCompanyTaxNumber.equals(taxNumberInput.getText().toString())) {
                if (!companyService.isTaxRateNumberUnique(taxNumberInput.getText().toString())) {
                    taxNumberInput.setError("Tax number must be unique!");
                    return;
                }
            }

            if (!companyService.isBankAccountNumberValid(bankAccountNumberInput.getText().toString())) {
                bankAccountNumberInput.setError("Bank account number must have 26 characters!");
                return;
            }

            company.getAddress().setAddress(addressInput.getText().toString());
            company.getAddress().setPostCode(postCodeInput.getText().toString());
            company.getAddress().setCity(cityInput.getText().toString());
            addressDao.update(company.getAddress());

            company.setName(companyNameInput.getText().toString());
            company.setTaxNumber(taxNumberInput.getText().toString());
            company.setBankAccountNumber(bankAccountNumberInput.getText().toString());
            companyDao.update(company);

            Intent intent = new Intent(this, MyCompaniesActivity.class);
            intent.putExtra("TOAST_MESSAGE", "The company has been edited!");
            startActivity(intent);
        }
    }
}
