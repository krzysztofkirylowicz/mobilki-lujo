package wooocash.pl.mobileinvoice.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Company;
import wooocash.pl.mobileinvoice.greendao.CompanyDao;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.main.App;
import wooocash.pl.mobileinvoice.util.CompaniesListAdapter;
import wooocash.pl.mobileinvoice.util.CompanyService;
import wooocash.pl.mobileinvoice.util.ValidationService;

public class ContractorsActivity extends AppCompatActivity {

    private ValidationService validationService;
    private CompaniesListAdapter companiesListAdapter;
    private CompanyDao companyDao;
    private CompanyService companyService;

    @BindView(R.id.contractorsListView)
    ListView contractorsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contractors);
        ButterKnife.bind(this);

        this.validationService = new ValidationService();
        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        this.companyDao = daoSession.getCompanyDao();
        this.companyService = new CompanyService(companyDao);

        showContractorsList();
        showToastMessage();
    }

    private void showContractorsList() {
        List<Company> contractors = companyService.findContractors();
        companiesListAdapter = new CompaniesListAdapter(getApplicationContext(), contractors);
        contractorsListView.setAdapter(companiesListAdapter);
        setOnItemClickListenerForContractorsListView();
    }

    private void setOnItemClickListenerForContractorsListView() {
        final Intent goToContractorDetails = new Intent(this, ContractorDetailsActivity.class);
        contractorsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToContractorDetails.putExtra("CONTRACTOR_ID", (long)view.getTag());
                startActivity(goToContractorDetails);
            }
        });
    }

    private void showToastMessage() {
        Intent intent = getIntent();
        String toastMessage = intent.getStringExtra("TOAST_MESSAGE");
        if (!validationService.isStringNullOrEmpty(toastMessage)) {
            Toast toast = Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
