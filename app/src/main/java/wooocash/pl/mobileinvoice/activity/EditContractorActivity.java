package wooocash.pl.mobileinvoice.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Company;
import wooocash.pl.mobileinvoice.greendao.AddressDao;
import wooocash.pl.mobileinvoice.greendao.CompanyDao;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.main.App;
import wooocash.pl.mobileinvoice.util.CompanyService;
import wooocash.pl.mobileinvoice.util.ValidationService;

public class EditContractorActivity extends AppCompatActivity {

    private AddressDao addressDao;
    private CompanyDao companyDao;
    private ValidationService validationService;
    private CompanyService companyService;
    private Company contractor;
    private String oldContractorTaxNumber;

    @BindView(R.id.contractorNameInput)
    EditText contractorNameInput;

    @BindView(R.id.taxtNumberInput)
    EditText taxNumberInput;

    @BindView(R.id.addressInput)
    EditText addressInput;

    @BindView(R.id.postCodeInput)
    EditText postCodeInput;

    @BindView(R.id.cityInput)
    EditText cityInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contractor);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Editing a contractor");

        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        this.addressDao = daoSession.getAddressDao();
        this.companyDao = daoSession.getCompanyDao();
        this.validationService = new ValidationService();
        this.companyService = new CompanyService(companyDao);
        this.contractor = companyService.findCompanyById(getContractorIdFromIntent());
        this.oldContractorTaxNumber = contractor.getTaxNumber();
        setUpInputsByValuesFromDB();
    }

    private long getContractorIdFromIntent() {
        Intent intent = getIntent();
        long contractorID = intent.getLongExtra("CONTRACTOR_ID", 0);
        return contractorID;
    }

    private void setUpInputsByValuesFromDB() {
        contractorNameInput.setText(contractor.getName());
        taxNumberInput.setText(contractor.getTaxNumber());
        addressInput.setText(contractor.getAddress().getAddress());
        postCodeInput.setText(contractor.getAddress().getPostCode());
        cityInput.setText(contractor.getAddress().getCity());
    }

    @OnClick(R.id.editContractorButton)
    public void editContractor() {
        if (validationService.isRequiredInputsFilled(contractorNameInput, taxNumberInput, addressInput,
                postCodeInput, cityInput)) {

            if (!companyService.isTaxNumberValid(taxNumberInput.getText().toString())) {
                taxNumberInput.setError("Tax number must have 10 characters!");
                return;
            }

            if (!oldContractorTaxNumber.equals(taxNumberInput.getText().toString())) {
                if (!companyService.isTaxRateNumberUnique(taxNumberInput.getText().toString())) {
                    taxNumberInput.setError("Tax number must be unique!");
                    return;
                }
            }

            contractor.getAddress().setAddress(addressInput.getText().toString());
            contractor.getAddress().setPostCode(postCodeInput.getText().toString());
            contractor.getAddress().setCity(cityInput.getText().toString());
            addressDao.update(contractor.getAddress());

            contractor.setName(contractorNameInput.getText().toString());
            contractor.setTaxNumber(taxNumberInput.getText().toString());
            companyDao.update(contractor);

            Intent intent = new Intent(this, ContractorsActivity.class);
            intent.putExtra("TOAST_MESSAGE", "The contractor has been edited!");
            startActivity(intent);
        }
    }
}
