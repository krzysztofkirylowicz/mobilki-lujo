package wooocash.pl.mobileinvoice.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Company;
import wooocash.pl.mobileinvoice.greendao.CompanyDao;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.main.App;
import wooocash.pl.mobileinvoice.util.CompaniesListAdapter;
import wooocash.pl.mobileinvoice.util.CompanyService;
import wooocash.pl.mobileinvoice.util.ValidationService;

public class MyCompaniesActivity extends AppCompatActivity {

    private ValidationService validationService;
    private CompaniesListAdapter companiesListAdapter;
    private CompanyDao companyDao;
    private CompanyService companyService;

    @BindView(R.id.addCompanyButton)
    FloatingActionButton addCompanyButton;

    @BindView(R.id.myCompaniesListView)
    ListView myCompaniesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_companies);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("My companies");

        this.validationService = new ValidationService();
        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        this.companyDao = daoSession.getCompanyDao();
        this.companyService = new CompanyService(companyDao);

        showMyCompaniesList();
        showToastMessage();
    }

    private void showMyCompaniesList() {
        List<Company> myCompanies = companyService.findMyCompanies();
        companiesListAdapter = new CompaniesListAdapter(getApplicationContext(), myCompanies);
        myCompaniesListView.setAdapter(companiesListAdapter);
        setOnItemClickListenerForMyCompaniesListView();
    }

    private void setOnItemClickListenerForMyCompaniesListView() {
        final Intent goToCompanyDetails = new Intent(this, CompanyDetailsActivity.class);
        myCompaniesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            goToCompanyDetails.putExtra("COMPANY_ID", (long)view.getTag());
            startActivity(goToCompanyDetails);
            }
        });
    }

    private void showToastMessage() {
        Intent intent = getIntent();
        String toastMessage = intent.getStringExtra("TOAST_MESSAGE");
        if (!validationService.isStringNullOrEmpty(toastMessage)) {
            Toast toast = Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @OnClick(R.id.addCompanyButton)
    public void addCompanyView() {
        Intent intent = new Intent(this, AddCompanyActivity.class);
        startActivity(intent);
    }
}
