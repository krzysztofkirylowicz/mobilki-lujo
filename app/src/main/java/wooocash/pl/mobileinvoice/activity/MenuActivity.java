package wooocash.pl.mobileinvoice.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import wooocash.pl.mobileinvoice.R;
import wooocash.pl.mobileinvoice.domain.Invoice;
import wooocash.pl.mobileinvoice.greendao.DaoSession;
import wooocash.pl.mobileinvoice.greendao.InvoiceDao;
import wooocash.pl.mobileinvoice.main.App;
import wooocash.pl.mobileinvoice.util.CompaniesListAdapter;
import wooocash.pl.mobileinvoice.util.CompanyService;
import wooocash.pl.mobileinvoice.util.InvoiceService;
import wooocash.pl.mobileinvoice.util.InvoicesListAdapter;
import wooocash.pl.mobileinvoice.util.ValidationService;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private InvoicesListAdapter invoicesListAdapter;
    private InvoiceDao invoiceDao;
    private InvoiceService invoiceService;
    private ValidationService validationService;

    @BindView(R.id.invoicesListView)
    ListView invoicesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        DaoSession daoSession = ((App)getApplication()).getDaoSession();
        this.invoiceDao = daoSession.getInvoiceDao();
        this.invoiceService = new InvoiceService(invoiceDao);
        this.validationService = new ValidationService();

        showInvoicesList();
        showToastMessage();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_companies) {
            Intent intent = new Intent(this, MyCompaniesActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_invoices) {
            Intent intent = new Intent(this, MenuActivity.class);
            startActivity(intent);
        }  else if (id == R.id.nav_contractors) {
            Intent intent = new Intent(this, ContractorsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showInvoicesList() {
        List<Invoice> invoices = invoiceService.findInvoices();
        invoicesListAdapter = new InvoicesListAdapter(getApplicationContext(), invoices);
        invoicesListView.setAdapter(invoicesListAdapter);
        setOnItemClickListenerForInvoicesListView();
    }

    private void setOnItemClickListenerForInvoicesListView() {
        final Intent goToInvoicesDetails = new Intent(this, InvoiceDetailsActivity.class);
        invoicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToInvoicesDetails.putExtra("INVOICE_ID", (long)view.getTag());
                startActivity(goToInvoicesDetails);
            }
        });
    }

    @OnClick(R.id.addInvoiceButton)
    public void addInvoiceView() {
        Intent intent = new Intent(this, AddInvoiceActivity.class);
        startActivity(intent);
    }

    private void showToastMessage() {
        Intent intent = getIntent();
        String toastMessage = intent.getStringExtra("TOAST_MESSAGE");
        if (!validationService.isStringNullOrEmpty(toastMessage)) {
            Toast toast = Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
